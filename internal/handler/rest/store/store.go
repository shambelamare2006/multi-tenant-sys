package store

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	rest "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest"
)

func (h *StoreH) GetStore(ctx *gin.Context) {}

func (h *StoreH) CreateStore(c *gin.Context) {
	var st *Store
	if err := c.ShouldBindJSON(&st); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}
	metadata, err := h.tk.ExtractTokenMetadata(c.Request)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	userId, err := h.rd.FetchAuth(metadata.TokenUuid)
	if err != nil {

		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	st.Owner = userId
	st.CreatedAt = time.Now()
	st.UpdatedAt = time.Now()

	store := &model.Store{
		OwnerID:    st.Owner,
		StoreName:  st.Name,
		StoreType:  st.Type,
		StoreImage: st.Image,
		Created_at: st.CreatedAt,
	}
	createdStore, err := h.StoreService.CreateStore(c, *store)
	if err != nil {
		c.Error(err)
		return
	}
	//create a policy rule for the owner to see, update this store
	policy := model.Policy{
		Role:   "OWNER",
		Tenant: createdStore.ID.String(),
		Resources: []string{
			"store",
		},
		Rights: []string{
			"read",
			"write",
		},
	}
	permission := model.Permission{
		Role:   "OWNER",
		User:   metadata.UserEmail,
		Tenant: createdStore.ID.String(),
	}
	errPolicy := casbiner.AddPolicy(policy, h.Adapter)
	if errPolicy != nil {
		c.Error(errPolicy)
		return
	}
	errPerm := casbiner.GiveRolePermission(permission, h.Adapter)
	if errPerm != nil {
		c.Error(errPerm)
		return
	}
	rest.Response(c, createdStore)

}
