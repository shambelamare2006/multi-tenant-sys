package store

import (
	"time"

	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/store"
)

//go:generate mockgen -destination=../../../../mocks/handlers/mock_StoreHandler.go -package=handler_mock github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/stores StoreHandler

type StoreHandler interface {
	CreateStore(ctx *gin.Context)
	GetStore(ctx *gin.Context)
}
type StoreH struct {
	StoreService store.Storer
	rd           auth.AuthInterface
	tk           auth.TokenInterface
	Adapter      *sqladapter.Adapter
}
type Config struct {
	StoreService store.Storer
	TokenRepo    auth.AuthInterface
	TokenService auth.TokenInterface
	Adapter      *sqladapter.Adapter
}

var casbiner = auth.NewCasbiner()

func Initialize(c *Config) StoreHandler {
	handler := &StoreH{
		StoreService: c.StoreService,
		rd:           c.TokenRepo,
		tk:           c.TokenService,
		Adapter:      c.Adapter,
	}
	return handler
}

type Store struct {
	Owner     string    `json:"owner"`
	Name      string    `json:"name"`
	Type      string    `json:"type"`
	Image     string    `json:"image"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
