package user

import (
	"github.com/gin-gonic/gin"
	_ "github.com/go-playground/validator/v10"

	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	rest "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest"
)

//signup handler
func (h *UserH) SignUp(c *gin.Context) {
	var req signupReq

	//bind incoming json to struct and check for validation error
	if err := c.BindJSON(&req); err != nil {
		c.Error(err)
		return
	}
	err := req.Validate()
	if err != nil {
		err = apperrors.ValidationFaild.Wrap(err, "one or more invalid inputs")
		c.Error(err)
		return
	}

	// transform req to model struct
	u := model.Customer{
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Email:     req.Email,
		Password:  req.Password,
		Phone:     req.Phone,
	}

	createdUser, err := h.UserService.Register(c, &u)
	if err != nil {
		c.Error(err)
		return
	}
	rest.Response(c, createdUser)

}
