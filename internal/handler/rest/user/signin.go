package user

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	rest "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest"
)

type loginUserRequest struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func (h *UserH) SignIn(ctx *gin.Context) {
	var req loginUserRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err = apperrors.IllegalArgument.Wrap(err, "binding faild")
		ctx.Error(err)
		return
	}
	user, err := h.UserService.Loggin(ctx, req.Email, req.Password)

	if err != nil {
		log.Printf("Error on login service:%v", err)

		ctx.Error(err)
		return
	}
	ts, err := h.tk.CreateToken(user.ID.String(), user.Email)

	if err != nil {
		log.Printf("Error create token:%v", err)
		ctx.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	saveErr := h.rd.CreateAuth(user.ID.String(), ts)

	if saveErr != nil {
		log.Printf("Error on redis service:%v", saveErr)

		ctx.JSON(http.StatusUnprocessableEntity, saveErr.Error())
		return
	}
	roles, perms, err := casbiner.GetRolesAndPermissions(user.Email, h.Adapter)

	if err != nil {
		log.Printf("Error on geting roles :%v", err)

		ctx.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	tokens := map[string]interface{}{
		"access_token":  ts.AccessToken,
		"refresh_token": ts.RefreshToken,
		"roles":         roles,
		"permissions":   perms,
	}
	rest.Response(ctx, tokens)
}

func (h *UserH) Refresh(c *gin.Context) {
	mapToken := map[string]string{}
	if err := c.ShouldBindJSON(&mapToken); err != nil {
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	refreshToken := mapToken["refresh_token"]

	//verify the token
	token, err := jwt.Parse(refreshToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("REFRESH_SECRET")), nil
	})
	//if there is an error, the token must have expired
	if err != nil {
		c.JSON(http.StatusUnauthorized, "Refresh token expired")
		return
	}
	//is token valid?
	if ok := token.Valid; !ok {
		c.JSON(http.StatusUnauthorized, "token not valid")
		return
	}
	//Since token is valid, get the uuid:
	claims, ok := token.Claims.(jwt.MapClaims) //the token claims should conform to MapClaims
	if ok && token.Valid {
		refreshUuid, ok := claims["refresh_uuid"].(string) //convert the interface to string
		if !ok {
			c.JSON(http.StatusUnprocessableEntity, err)
			return
		}
		userId, roleOk := claims["user_id"].(string)

		if !roleOk {
			c.JSON(http.StatusUnprocessableEntity, "unauthorized")
			return
		}
		userEmail, roleOk := claims["user_emal"].(string)
		if !roleOk {
			c.JSON(http.StatusUnprocessableEntity, "unauthorized")
			return
		}
		//Delete the previous Refresh Token
		delErr := h.rd.DeleteRefresh(refreshUuid)
		if delErr != nil { //if any goes wrong
			c.JSON(http.StatusUnauthorized, "unauthorized")
			return
		}
		//Create new pairs of refresh and access tokens
		ts, createErr := h.tk.CreateToken(userId, userEmail)
		if createErr != nil {
			c.JSON(http.StatusForbidden, createErr.Error())
			return
		}
		//save the tokens metadata to redis
		saveErr := h.rd.CreateAuth(userId, ts)
		if saveErr != nil {
			c.JSON(http.StatusForbidden, saveErr.Error())
			return
		}
		tokens := map[string]string{
			"access_token":  ts.AccessToken,
			"refresh_token": ts.RefreshToken,
		}
		c.JSON(http.StatusCreated, tokens)
	} else {
		c.JSON(http.StatusUnauthorized, "refresh expired")
	}
}
