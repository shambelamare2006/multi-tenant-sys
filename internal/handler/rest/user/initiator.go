package user

import (
	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/user"
)

//go:generate mockgen -destination=../../../../mocks/handlers/mock_UserHandler.go -package=handler_mock github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/users UserHandler

type UserH struct {
	UserService user.LoggerRegisterer
	rd          auth.AuthInterface
	tk          auth.TokenInterface
	Adapter     *sqladapter.Adapter
}
type UserHandler interface {
	SignUp(ctx *gin.Context)
	SignIn(ctx *gin.Context)
	SignUpSystemAdmin(c *gin.Context)
	SignInSystemAdmin(c *gin.Context)
	SignInSuperAdmin(c *gin.Context)
	SignUpSuperAdmin(c *gin.Context)
}
type Config struct {
	UserService  user.LoggerRegisterer
	TokenRepo    auth.AuthInterface
	TokenService auth.TokenInterface
	Adapter      *sqladapter.Adapter
}

var casbiner = auth.NewCasbiner()

func Initialize(c *Config) UserHandler {
	handler := &UserH{
		UserService: c.UserService,
		rd:          c.TokenRepo,
		tk:          c.TokenService,
		Adapter:     c.Adapter,
	}
	return handler
}

type signupReq struct {
	FirstName string `json:"first_name" `
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Password  string `json:"password"`
}
