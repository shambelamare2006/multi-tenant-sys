package user

import (
	"log"
	"net/http"

	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	rest "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest"
)

func (h *UserH) SignInSuperAdmin(ctx *gin.Context) {
	var req loginUserRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err = apperrors.IllegalArgument.Wrap(err, "binding faild")
		ctx.Error(err)
		return
	}
	user, err := h.UserService.Loggin(ctx, req.Email, req.Password)

	if err != nil {
		log.Printf("Error on login service:%v", err)

		ctx.Error(err)
		return
	}
	ts, err := h.tk.CreateToken(user.ID.String(), user.Email)

	if err != nil {
		log.Printf("Error create token:%v", err)
		ctx.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	saveErr := h.rd.CreateAuth(user.ID.String(), ts)

	if saveErr != nil {
		log.Printf("Error on redis service:%v", saveErr)

		ctx.JSON(http.StatusUnprocessableEntity, saveErr.Error())
		return
	}
	roles, perms, err := casbiner.GetRolesAndPermissions(user.Email, h.Adapter)

	if err != nil {
		log.Printf("Error on geting roles :%v", err)

		ctx.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	tokens := map[string]interface{}{
		"access_token":  ts.AccessToken,
		"refresh_token": ts.RefreshToken,
		"roles":         roles,
		"permissions":   perms,
	}
	rest.Response(ctx, tokens)
}

//signup handler
func (h *UserH) SignUpSuperAdmin(c *gin.Context) {
	var req signupReq

	//bind incoming json to struct and check for validation error
	if err := c.BindJSON(&req); err != nil {
		c.Error(err)
		return
	}
	err := req.Validate()
	if err != nil {
		err = apperrors.ValidationFaild.Wrap(err, "one or more invalid inputs")
		c.Error(err)
		return
	}

	// transform req to model struct
	u := model.Customer{
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Email:     req.Email,
		Password:  req.Password,
		Phone:     req.Phone,
	}

	createdUser, err := h.UserService.Register(c, &u)
	if err != nil {
		c.Error(err)
		return
	}
	GiveSupperAdminRights(c, &createdUser, h.Adapter)
	GiveSupperAdminPermissions(c, &createdUser, h.Adapter)
	rest.Response(c, createdUser)

}
func GiveSupperAdminRights(c *gin.Context, u *model.Customer, a *sqladapter.Adapter) {
	policy := &model.Policy{
		Role:   "SUPERADMIN",
		Tenant: "systemID",
		Resources: []string{
			"*",
			"all",
		},
		Rights: []string{
			"read",
			"write",
		},
	}

	policyerr := casbiner.AddPolicy(*policy, a)
	if policyerr != nil {
		c.Error(policyerr)
		return
	}

}
func GiveSupperAdminPermissions(c *gin.Context, u *model.Customer, a *sqladapter.Adapter) {
	permission := &model.Permission{
		Role:   "SUPERADMIN",
		User:   u.Email,
		Tenant: "systemID",
	}
	permerr := casbiner.GiveRolePermission(*permission, a)
	if permerr != nil {
		c.Error(permerr)
		return
	}
}
