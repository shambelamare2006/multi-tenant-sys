package user

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	rest "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
)

var casbinner = auth.NewCasbiner()

//signup handler
func (h *UserH) SignUpSystemAdmin(c *gin.Context) {
	var req signupReq

	//bind incoming json to struct and check for validation error
	if err := c.BindJSON(&req); err != nil {
		c.Error(err)
		return
	}
	err := req.Validate()
	if err != nil {
		err = apperrors.ValidationFaild.Wrap(err, "one or more invalid inputs")
		c.Error(err)
		return
	}

	// transform req to model struct
	u := model.Customer{
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Email:     req.Email,
		Password:  req.Password,
		Phone:     req.Phone,
	}

	createdUser, err := h.UserService.Register(c, &u)
	if err != nil {
		c.Error(err)
		return
	}
	policy := &model.Policy{
		Role:   "SYSTEMADMIN",
		Tenant: "systemID",
		Resources: []string{
			"*",
			"tenants",
		},
		Rights: []string{
			"read",
			"write",
		},
	}
	permission := &model.Permission{
		User:   createdUser.Email,
		Role:   "SYSTEMADMIN",
		Tenant: "systemID",
	}
	policyerr := casbinner.AddPolicy(*policy, h.Adapter)
	if policyerr != nil {
		c.Error(policyerr)
		return
	}
	permerr := casbinner.GiveRolePermission(*permission, h.Adapter)
	if permerr != nil {
		c.Error(permerr)
		return
	}
	rest.Response(c, createdUser)

}

func (h *UserH) SignInSystemAdmin(ctx *gin.Context) {
	var req loginUserRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err = apperrors.IllegalArgument.Wrap(err, "binding faild")
		ctx.Error(err)
		return
	}
	user, err := h.UserService.Loggin(ctx, req.Email, req.Password)

	if err != nil {
		log.Printf("Error on login service:%v", err)

		ctx.Error(err)
		return
	}
	ts, err := h.tk.CreateToken(user.ID.String(), user.Email)

	if err != nil {
		log.Printf("Error create token:%v", err)
		ctx.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	saveErr := h.rd.CreateAuth(user.ID.String(), ts)

	if saveErr != nil {
		log.Printf("Error on redis service:%v", saveErr)

		ctx.JSON(http.StatusUnprocessableEntity, saveErr.Error())
		return
	}
	roles, perms, err := casbinner.GetRolesAndPermissions(user.Email, h.Adapter)

	if err != nil {
		log.Printf("Error on geting roles :%v", err)

		ctx.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	tokens := map[string]interface{}{
		"access_token":  ts.AccessToken,
		"refresh_token": ts.RefreshToken,
		"roles":         roles,
		"permissions":   perms,
	}
	rest.Response(ctx, tokens)

}
