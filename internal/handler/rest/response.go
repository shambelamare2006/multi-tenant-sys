package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Response(c *gin.Context, data interface{}) {
	c.JSON(http.StatusCreated, response{
		Data: data,
	})
}

type response struct {
	Data interface{} `json:"data"`
}
