package injector

import (
	"github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/store"
	"github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/tenant"
	"github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/user"
)

type handlerConfigs struct {
	tenant.TenantH
	tenant.ItemH
	tenant.RoleH
	user.UserH
	store.StoreH
}
type Handler interface {
	tenant.TenantHandler
	tenant.RoleHandler
	tenant.ItemHandler
	user.UserHandler
	store.StoreHandler
}

func NewRestHandler() Handler {
	return &handlerConfigs{}
}
