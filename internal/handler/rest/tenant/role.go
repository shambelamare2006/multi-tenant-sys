package tenant

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	rest "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest"
)

type rolePermissionRequest struct {
	RoleTitle string   `json:"role_title"`
	RoleEmail string   `json:"role_email"`
	TenantId  string   `json:"tenant_id"`
	Resources []string `json:"resources"`
	Rights    []string `json:"rights"`
}

func (h *RoleH) CreateRole(c *gin.Context) {
	var req *Role
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}
	tenantID := c.Param("tenant_id")
	mrole := &model.Role{
		Title:        req.Title,
		TenantID:     tenantID,
		RoleEmail:    req.RoleEmail,
		RolePhone:    req.RolePhone,
		RolePassword: req.RolePassword,
		Status:       req.Status,
	}
	registeredRole, err := h.RoleService.CreateRole(c, *mrole)
	if err != nil {
		c.Error(err)
		return
	}

	rest.Response(c, registeredRole)
}
func (r *RoleH) UpdateRole(c *gin.Context) {

}

func (h *RoleH) LoggInRole(c *gin.Context) {
	var req loginUserRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		err = apperrors.IllegalArgument.Wrap(err, "binding faild")
		c.Error(err)
		return
	}
	role, err := h.RoleService.LoggInRole(c, req.Email, req.Password)

	if err != nil {
		log.Printf("Error on login service:%v", err)

		c.Error(err)
		return
	}
	ts, err := h.tk.CreateToken(role.ID.String(), role.RoleEmail)

	if err != nil {
		log.Printf("Error create token:%v", err)
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	saveErr := h.rd.CreateAuth(role.ID.String(), ts)

	if saveErr != nil {
		log.Printf("Error on redis service:%v", saveErr)

		c.JSON(http.StatusUnprocessableEntity, saveErr.Error())
		return
	}
	log.Printf("Role Email:%v", role.RoleEmail)
	roles, perms, err := h.casbiner.GetRolesAndPermissions(role.RoleEmail, h.Adapter)

	if err != nil {
		log.Printf("Error on geting roles :%v", err)

		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	tokens := map[string]interface{}{
		"access_token":  ts.AccessToken,
		"refresh_token": ts.RefreshToken,
		"roles":         roles,
		"permissions":   perms,
	}
	rest.Response(c, tokens)

}

func (h RoleH) AddPermissionToRole(c *gin.Context) {
	var req *rolePermissionRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}
	//add policy for the Admin of this registered tanant
	policy := &model.Policy{
		Role:      req.RoleTitle,
		Tenant:    req.TenantId,
		Resources: req.Resources,
		Rights:    req.Rights,
	}
	permission := &model.Permission{
		Role:   req.RoleTitle,
		User:   req.RoleEmail,
		Tenant: req.TenantId,
	}
	errPolicy := h.casbiner.AddPolicy(*policy, h.Adapter)
	if errPolicy != nil {
		c.Error(errPolicy)
		return
	}
	errPerm := h.casbiner.GiveRolePermission(*permission, h.Adapter)
	if errPerm != nil {
		c.Error(errPerm)
		return
	}
}

func (h RoleH) UpdatePermissionOfRole(c *gin.Context) {
	var req *rolePermissionRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}
	//add policy for the Admin of this registered tanant
	policy := &model.Policy{
		Role:      req.RoleTitle,
		Tenant:    req.TenantId,
		Resources: req.Resources,
		Rights:    req.Rights,
	}
	permission := &model.Permission{
		Role:   req.RoleTitle,
		User:   req.RoleEmail,
		Tenant: req.TenantId,
	}
	errPolicy := h.casbiner.AddPolicy(*policy, h.Adapter)
	if errPolicy != nil {
		c.Error(errPolicy)
		return
	}
	errPerm := h.casbiner.GiveRolePermission(*permission, h.Adapter)
	if errPerm != nil {
		c.Error(errPerm)
		return
	}
}

func (h RoleH) UpdateStatusOfRole(c *gin.Context) {
	var req *rolePermissionRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}
	//add policy for the Admin of this registered tanant
	policy := &model.Policy{
		Role:      req.RoleTitle,
		Tenant:    req.TenantId,
		Resources: req.Resources,
		Rights:    req.Rights,
	}
	permission := &model.Permission{
		Role:   req.RoleTitle,
		User:   req.RoleEmail,
		Tenant: req.TenantId,
	}
	errPolicy := h.casbiner.AddPolicy(*policy, h.Adapter)
	if errPolicy != nil {
		c.Error(errPolicy)
		return
	}
	errPerm := h.casbiner.GiveRolePermission(*permission, h.Adapter)
	if errPerm != nil {
		c.Error(errPerm)
		return
	}
}

func (h RoleH) GetRolesOfDomain(c *gin.Context) {
	var req *rolePermissionRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}
	//add policy for the Admin of this registered tanant
	policy := &model.Policy{
		Role:      req.RoleTitle,
		Tenant:    req.TenantId,
		Resources: req.Resources,
		Rights:    req.Rights,
	}
	permission := &model.Permission{
		Role:   req.RoleTitle,
		User:   req.RoleEmail,
		Tenant: req.TenantId,
	}
	errPolicy := h.casbiner.AddPolicy(*policy, h.Adapter)
	if errPolicy != nil {
		c.Error(errPolicy)
		return
	}
	errPerm := h.casbiner.GiveRolePermission(*permission, h.Adapter)
	if errPerm != nil {
		c.Error(errPerm)
		return
	}
}
