package tenant

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	rest "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest"
)

func (h *TenantH) RegisterTenant(c *gin.Context) {
	var t *Tenant
	if err := c.ShouldBindJSON(&t); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}

	mtenant := &model.Tenant{
		TenantName:    t.TenantName,
		TenantType:    t.TenantType,
		AdminName:     t.AdminName,
		AdminEmail:    t.AdminEmail,
		AdminPhone:    t.AdminPhone,
		AdminPassword: t.AdminPassword,
	}
	registeredTenant, err := h.TenantService.Register(c, mtenant)
	if err != nil {
		c.Error(err)
		return
	}
	//add policy for the Admin of this registered tanant
	policy := &model.Policy{
		Role:   "ADMIN",
		Tenant: registeredTenant.ID.String(),
		Resources: []string{
			"roles",
			"items",
		},
		Rights: []string{
			"read",
			"write",
		},
	}
	permission := &model.Permission{
		Role:   "ADMIN",
		User:   registeredTenant.AdminEmail,
		Tenant: registeredTenant.ID.String(),
	}
	errPolicy := h.casbiner.AddPolicy(*policy, h.Adapter)
	if errPolicy != nil {
		c.Error(errPolicy)
		return
	}
	errPerm := h.casbiner.GiveRolePermission(*permission, h.Adapter)
	if errPerm != nil {
		c.Error(errPerm)
		return
	}
	rest.Response(c, registeredTenant)

}

type loginUserRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (h *TenantH) LogginTenantAdmin(c *gin.Context) {
	var req loginUserRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		err = apperrors.IllegalArgument.Wrap(err, "binding faild")
		c.Error(err)
		return
	}
	tenant, err := h.TenantService.LogginTenantAdmin(c, req.Email, req.Password)

	if err != nil {
		log.Printf("Error on login service:%v", err)

		c.Error(err)
		return
	}
	ts, err := h.tk.CreateToken(tenant.ID.String(), tenant.AdminEmail)

	if err != nil {
		log.Printf("Error create token:%v", err)
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	saveErr := h.rd.CreateAuth(tenant.ID.String(), ts)

	if saveErr != nil {
		log.Printf("Error on redis service:%v", saveErr)

		c.JSON(http.StatusUnprocessableEntity, saveErr.Error())
		return
	}
	roles, perms, err := h.casbiner.GetRolesAndPermissions(tenant.AdminEmail, h.Adapter)

	if err != nil {
		log.Printf("Error on geting roles :%v", err)

		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	tokens := map[string]interface{}{
		"access_token":  ts.AccessToken,
		"refresh_token": ts.RefreshToken,
		"roles":         roles,
		"permissions":   perms,
	}
	rest.Response(c, tokens)

}
