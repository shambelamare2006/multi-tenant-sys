package tenant

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	rest "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest"
)

func (h *ItemH) CreateItem(c *gin.Context) {
	var it *Item
	if err := c.ShouldBindJSON(&it); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}

	mitem := &model.Item{
		ItemName:        it.ItemName,
		ItemType:        it.ItemType,
		ItemDescription: it.ItemDescription,
		Status:          it.Status,
		Owner:           it.Owner,
	}
	registeredItem, err := h.ItemService.CreateItem(c, *mitem)
	if err != nil {
		c.Error(err)
		return
	}
	rest.Response(c, registeredItem)
}
func (r *ItemH) UpdateItem(c *gin.Context) {

}
func (r *ItemH) UpdateItemStatus(c *gin.Context) {

}
