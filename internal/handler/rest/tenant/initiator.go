package tenant

import (
	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/item"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/role"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/tenant"
)

//go:generate mockgen -destination=../../../../mocks/handlers/mock_RoleHandler.go -package=handler_mock github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/tenants RoleHandler
//go:generate mockgen -destination=../../../../mocks/handlers/mock_ItemHandler.go -package=handler_mock github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/tenants ItemHandler
//go:generate mockgen -destination=../../../../mocks/handlers/mock_TenantHandler.go -package=handler_mock github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/tenants TenantHandler

type TenantHandler interface {
	RegisterTenant(c *gin.Context)
	LogginTenantAdmin(c *gin.Context)
}

type RoleHandler interface {
	CreateRole(c *gin.Context)
	UpdateRole(c *gin.Context)
	UpdateStatusOfRole(c *gin.Context)
	LoggInRole(c *gin.Context)
}
type ItemHandler interface {
	CreateItem(c *gin.Context)
	UpdateItem(c *gin.Context)
	UpdateItemStatus(c *gin.Context)
}
type TenantH struct {
	TenantService tenant.TenantService
	rd            auth.AuthInterface
	tk            auth.TokenInterface
	casbiner      auth.Casbiner
	Adapter       *sqladapter.Adapter
}
type RoleH struct {
	RoleService role.RoleInfo
	rd          auth.AuthInterface
	tk          auth.TokenInterface
	casbiner    auth.Casbiner
	Adapter     *sqladapter.Adapter
}
type ItemH struct {
	ItemService item.ItemInfo
	rd          auth.AuthInterface
	tk          auth.TokenInterface
	casbiner    auth.Casbiner
	Adapter     *sqladapter.Adapter
}

type Config struct {
	TenantService tenant.TenantService
	RoleService   role.RoleInfo
	ItemService   item.ItemInfo
	TokenRepo     auth.AuthInterface
	TokenService  auth.TokenInterface
	Adapter       *sqladapter.Adapter
}

func Initialize(c *Config) (TenantHandler, RoleHandler, ItemHandler) {
	tenanthandler := &TenantH{
		TenantService: c.TenantService,
		rd:            c.TokenRepo,
		tk:            c.TokenService,
		Adapter:       c.Adapter,
	}
	rolehandler := &RoleH{
		RoleService: c.RoleService,
		rd:          c.TokenRepo,
		tk:          c.TokenService,
		Adapter:     c.Adapter,
	}
	itemhandler := &ItemH{
		ItemService: c.ItemService,
		rd:          c.TokenRepo,
		tk:          c.TokenService,
		Adapter:     c.Adapter,
	}
	return tenanthandler, rolehandler, itemhandler
}

type Tenant struct {
	TenantName    string `json:"tenant_name"`
	TenantType    string `json:"tenant_type"`
	AdminName     string `json:"admin_name"`
	AdminEmail    string `json:"admin_email"`
	AdminPhone    string `json:"admin_phone"`
	AdminPassword string `json:"admin_password"`
}

type Item struct {
	ID              uuid.UUID `json:"id"`
	Owner           string    `json:"owner"`
	ItemName        string    `json:"item_name"`
	ItemType        string    `json:"item_type"`
	ItemDescription string    `json:"item_description"`
	Status          string    `json:"status"`
}

type Role struct {
	ID           uuid.UUID `json:"id"`
	Title        string    `json:"title"`
	TenantID     string    `json:"tenant_id"`
	RoleEmail    string    `json:"role_email"`
	RolePhone    string    `json:"role_phone"`
	RolePassword string    `json:"role_password"`
	Status       string    `json:"status"`
}
