package user_test

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/user"
	user_mock "github.com/shambel-amare/multi-tenant-sys/mocks/users"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

func TestUserServices(t *testing.T) {
	suite.Run(t, new(UserServiceTestSuite))
}

type UserServiceTestSuite struct {
	suite.Suite
	userRepo  *user_mock.MockUserRepository
	underTest user.LoggerRegisterer
}

// setup the test state
func (suite *UserServiceTestSuite) SetupTest() {
	mockCtrl := gomock.NewController(suite.T())
	defer mockCtrl.Finish()
	suite.userRepo = user_mock.NewMockUserRepository(mockCtrl)
	service := user.NewUserService(suite.userRepo)
	suite.underTest = service
}

type Format struct {
	Request  *model.Customer
	Response *db.User
}
type loginFormat struct {
	Email    string
	Password string
}

var form = Format{
	Request: &model.Customer{
		FirstName: "user1",
		LastName:  "user",
		Email:     "user1@gmail.com",
		Password:  "user1234",
		Phone:     "0983327298",
	},
	Response: &db.User{
		FirstName: "user1",
		LastName:  "user",
		Email:     "user1@gmail.com",
		Phone:     "0983327298",
	},
}

func (suite *UserServiceTestSuite) TestUserSignUpShouldSucceed() {
	//User is not a registered user
	suite.userRepo.EXPECT().GetUser(context.Background(), gomock.AssignableToTypeOf(model.Customer{}.Email)).Return(db.User{}, nil).Times(1)
	suite.userRepo.EXPECT().CreateUser(gomock.Any(), gomock.Any()).Return(*form.Response, nil).Times(1)
	user, err := suite.underTest.Register(context.Background(), form.Request)
	//Assertions
	require.Equal(suite.T(), user.Email, form.Response.Email)
	require.NoError(suite.T(), err)
	//assert password is not returned
	require.Empty(suite.T(), user.Password)
}
func (suite *UserServiceTestSuite) TestUserSignUpShouldFail() {
	var MockError error = nil
	//User is a registered user

	targetErr := apperrors.DuplicateUser.Wrap(MockError, "user already exist")
	suite.userRepo.EXPECT().GetUser(context.Background(), gomock.AssignableToTypeOf(model.Customer{}.Email)).Return(*form.Response, nil).Times(1)
	suite.userRepo.EXPECT().CreateUser(gomock.Any(), gomock.Any()).Return(db.User{}, nil).Times(1)
	user, err := suite.underTest.Register(context.Background(), form.Request)
	//Assertions
	require.Zero(suite.T(), user.Email)
	require.Equal(suite.T(), "", user.Email)

	require.Error(suite.T(), err)
	require.EqualError(suite.T(), err, targetErr.Error())
	//assert password is not returned
	require.Empty(suite.T(), user.Password)
}
func (suite *UserServiceTestSuite) TestLoginServiceShouldFail() {
	loginform := loginFormat{
		Email:    form.Request.Email,
		Password: form.Request.Password,
	}
	//Testing correct loggin
	suite.userRepo.EXPECT().GetUser(context.Background(), gomock.AssignableToTypeOf(model.Customer{}.Email)).Return(*form.Response, nil).Times(1)

	user, err := suite.underTest.Loggin(context.Background(), loginform.Email, loginform.Password)
	//Assertions
	require.Error(suite.T(), err)
	//assert password is not returned
	require.Empty(suite.T(), user.Password)

}
func (suite *UserServiceTestSuite) TestLoginServiceShouldSucceed() {
	//Testing incorrect loggin
	loginform := loginFormat{
		Email:    form.Request.Email,
		Password: form.Request.Password,
	}
	MockError := errors.New("no user registered")
	targetErr := apperrors.IllegalArgument.Wrap(MockError, "No user exists with this email, please register first")
	suite.userRepo.EXPECT().GetUser(context.Background(), gomock.AssignableToTypeOf(model.Customer{}.Email)).Return(db.User{}, MockError).Times(1)

	user, err := suite.underTest.Loggin(context.Background(), loginform.Email, loginform.Password)
	//Assertions
	require.ErrorIs(suite.T(), err, targetErr)
	//assert no user returned
	require.Empty(suite.T(), user)
	require.IsType(suite.T(), model.Customer{}, user)
}
