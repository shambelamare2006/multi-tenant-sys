package user

import (
	"context"
	"log"

	"github.com/joomcode/errorx"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"

	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
)

func (s *userService) Loggin(ctx context.Context, email, password string) (model.Customer, error) {
	uFetched, err := s.Repo.GetUser(ctx, email)

	if err != nil {
		//wrap the error with errorx?
		return model.Customer{}, apperrors.IllegalArgument.Wrap(err, "No user exists with this email, please register first")
	}

	matchErr := auth.CheckPassword(password, uFetched.Password)

	if matchErr != nil {
		return model.Customer{}, apperrors.IllegalArgument.Wrap(matchErr, "Invalid email and password combination")
	}
	res := Response_struct(uFetched)
	return res, nil
}

func (s *userService) Register(ctx context.Context, u *model.Customer) (model.Customer, error) {
	fetcheduser, err := s.Repo.GetUser(ctx, u.Email)

	if err == nil && fetcheduser.Email != "" {
		//wrap the error with errorx?
		return model.Customer{}, apperrors.DuplicateUser.Wrap(err, "user already exist")
	}

	hashedPassword, err := auth.HashPassword(u.Password)
	u.Password = hashedPassword
	if err != nil {
		log.Printf("error hashing password:%v", err)
		return model.Customer{}, errorx.InternalError.Wrap(err, "error hashing password")
	}
	user := Request_struct(u)
	createdUser, err := s.Repo.CreateUser(ctx, *user)
	if err != nil {
		return model.Customer{}, err
	}

	customer := Response_struct(createdUser)
	return customer, nil
}
