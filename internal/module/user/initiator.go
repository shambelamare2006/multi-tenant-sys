package user

import (
	"context"

	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	persistence "github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence"
)

//go:generate mockgen -destination=../../../mocks/users/mock_Registerer.go -package=user_mock github.com/shambel-amare/multi-tenant-sys/internal/module/users LoggerRegisterer

type userService struct {
	Repo persistence.UserRepository
}
type Registerer interface {
	Register(ctx context.Context, u *model.Customer) (model.Customer, error)
}

type Logger interface {
	Loggin(ctx context.Context, email, password string) (model.Customer, error)
}
type LoggerRegisterer interface {
	Logger
	Registerer
}

func NewUserService(repo persistence.UserRepository) LoggerRegisterer {
	return &userService{
		Repo: repo,
	}
}
func Request_struct(u *model.Customer) *db.CreateUserParams {
	user := &db.CreateUserParams{
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Email:     u.Email,
		Password:  u.Password,
		Phone:     u.Phone,
	}
	return user

}
func Response_struct(u db.User) model.Customer {
	customer := model.Customer{
		ID:        u.ID,
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Email:     u.Email,
		Phone:     u.Phone,
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
	}
	return customer

}
