package auth

import (
	"errors"
	"fmt"
	"log"

	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/casbin/casbin/v2"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
)

//go:generate mockgen -destination=../../../mocks/auth/mock_Casbiner.go -package=auth_mock github.com/shambel-amare/multi-tenant-sys/internal/module/auth Casbiner

type casbinService struct{}
type Casbiner interface {
	GetRolesAndPermissions(userEmail string, adapter *sqladapter.Adapter) ([][]string, [][][]string, error)
	AddPolicy(p model.Policy, a *sqladapter.Adapter) error
	GiveRolePermission(p model.Permission, adapter *sqladapter.Adapter) error
}

func NewCasbiner() Casbiner {
	return &casbinService{}
}
func (c *casbinService) GetRolesAndPermissions(userEmail string, adapter *sqladapter.Adapter) ([][]string, [][][]string, error) {
	// Load model configuration file and policy store adapter
	enforcer, err := getEnforcer(adapter)
	if err != nil {
		log.Printf("faild to initiate enforcer:%v", err)
		return nil, nil, fmt.Errorf("faild to initiate enforcer: %w", err)
	}
	var roles [][]string
	var perms [][][]string
	// get all domains that the user is part of
	domains, err := enforcer.GetDomainsForUser(userEmail)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to get user permissions: %w", err)
	}
	for _, domain := range domains {

		roles = append(roles, enforcer.GetRolesForUserInDomain(userEmail, domain))
		if err != nil {
			return nil, nil, fmt.Errorf("failed to get user roles: %w", err)
		}
		perms = append(perms, enforcer.GetPermissionsForUserInDomain(userEmail, domain))
	}
	return roles, perms, err
}
func (c *casbinService) AddPolicy(p model.Policy, a *sqladapter.Adapter) error {
	if len(p.Rights) < 1 {
		err := errors.New("no right paramater provided:%v")
		return err
	}
	if len(p.Resources) < 1 {
		err := errors.New("no resource paramater provided:%v")
		return err
	}
	// Load model configuration file and policy store adapter

	enforcer, err := getEnforcer(a)
	if err != nil {
		log.Printf("faild to initiate enforcer:%v", err)
		return err
	}
	for _, resource := range p.Resources {

		for _, right := range p.Rights {

			ok1, err1 := enforcer.AddPolicy(p.Role, p.Tenant, resource, right)
			if !ok1 && err1 != nil {
				log.Printf("duplicate policy, policy for domain: %s, and owner: %s exists", p.Tenant, p.Role)
				return err1
			}
		}

	}
	return nil
}

func (c *casbinService) GiveRolePermission(p model.Permission, adapter *sqladapter.Adapter) error {
	enforcer, err := getEnforcer(adapter)
	if err != nil {
		log.Printf("faild to initiate enforcer:%v", err)
		return err
	}
	//add grouping policy: user is owner of the created store
	ok, err := enforcer.AddGroupingPolicy(p.User, p.Role, p.Tenant)
	if !ok {
		log.Printf("duplicate grouping policy, policy for domain:%s, and owner:%s exists", p.Tenant, p.User)
		return err
	}
	if err != nil {
		return fmt.Errorf("failed to add grouping policy to DB:%w for domain:%s", err, p.Tenant)
	}

	return nil
}

func getEnforcer(adapter *sqladapter.Adapter) (*casbin.Enforcer, error) {
	// Load model configuration file and policy store adapter
	enforcer, err := casbin.NewEnforcer("config/rbac_model.conf", adapter)
	if err != nil {
		return nil, fmt.Errorf("failed to open new enforcer: %w", err)
	}
	// Load policies from DB dynamically
	err = enforcer.LoadPolicy()
	if err != nil {
		return nil, fmt.Errorf("failed to load policy from DB: %w", err)
	}
	return enforcer, nil
}
