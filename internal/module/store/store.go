package store

import (
	"context"

	"github.com/google/uuid"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
)

func (s *storeService) CreateStore(c context.Context, store model.Store) (model.Store, error) {

	//one owner can create as many store as he want
	storeparams := createStoreParams(store)

	createdStore, err := s.Repo.CreateStore(c, *storeparams)
	if err != nil {
		return model.Store{}, err
	}
	return *singleStoreResponse(createdStore), nil
}
func (s *storeService) GetStoreByID(c context.Context, id uuid.UUID) (model.Store, error) {
	store, err := s.Repo.GetStoreByID(c, id)
	if err != nil {
		return model.Store{}, err
	}
	return *singleStoreResponse(store), nil

}
func (s *storeService) GetOwnerStore(c context.Context, owner string) ([]model.Store, error) {
	store, err := s.Repo.GetOwnerStore(c, owner)
	if err != nil {
		return []model.Store{}, err
	}
	return *storeResponse(store), nil

}
