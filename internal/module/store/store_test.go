package store_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/store"
	store_mock "github.com/shambel-amare/multi-tenant-sys/mocks/stores"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

func TestStoreServices(t *testing.T) {
	suite.Run(t, new(StoreServiceTestSuite))
	// t.Parallel()
}

type StoreServiceTestSuite struct {
	suite.Suite
	StoreRepo *store_mock.MockStoreRepository
	underTest store.Storer
}

func (suite *StoreServiceTestSuite) SetupTest() {
	mockCtrl := gomock.NewController(suite.T())
	defer mockCtrl.Finish()
	suite.StoreRepo = store_mock.NewMockStoreRepository(mockCtrl)
	service := store.NewStoreService(suite.StoreRepo)
	suite.underTest = service
}

type Format struct {
	Request   model.Store
	Response  db.Store
	Responses []db.Store
}
type GetFormat struct {
	ID    uuid.UUID
	Owner string
}

var id = uuid.New()
var form = Format{
	Request: model.Store{
		StoreName:  "store1",
		StoreType:  "retail",
		StoreImage: "image1",
		OwnerID:    "1",
	},
	Response: db.Store{
		ID:         id,
		StoreName:  "store1",
		StoreType:  "retail",
		StoreImage: "image1",
		OwnerID:    "1",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	},
	Responses: []db.Store{
		{
			ID:         id,
			StoreName:  "store1",
			StoreType:  "retail",
			StoreImage: "image1",
			OwnerID:    "1",
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
		},
		{
			ID:         id,
			StoreName:  "store2",
			StoreType:  "retail",
			StoreImage: "image2",
			OwnerID:    "2",
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
		},
	},
}

func (suite *StoreServiceTestSuite) TestCreateStoreShouldPass() {
	// t := suite.T()
	// t.Parallel()
	//Store is not a Created Before
	suite.T().Log("Testing Create store success scenario ...")
	suite.StoreRepo.EXPECT().CreateStore(gomock.Any(), gomock.Any()).Return(form.Response, nil).Times(1)
	store, err := suite.underTest.CreateStore(context.Background(), form.Request)
	//Assertions
	require.Equal(suite.T(), store.OwnerID, form.Response.OwnerID)
	require.NoError(suite.T(), err)
}

var getForm = GetFormat{
	ID:    form.Request.ID,
	Owner: form.Request.OwnerID,
}

func (suite *StoreServiceTestSuite) TestGetStoreByOwnerShouldPass() {
	// t := suite.T()
	// t.Parallel()
	suite.T().Log("Testing fetch store by owner success scenario ...")

	suite.StoreRepo.EXPECT().GetOwnerStore(context.Background(), gomock.AssignableToTypeOf(model.Store{}.OwnerID)).Return(form.Responses, nil).Times(1)
	store, err := suite.underTest.GetOwnerStore(context.Background(), form.Request.OwnerID)
	//Assertions
	require.NoError(suite.T(), err)
	require.NotZero(suite.T(), store)

	require.Equal(suite.T(), getForm.Owner, store[0].OwnerID)
	require.Equal(suite.T(), "2", store[1].OwnerID)

	require.Equal(suite.T(), form.Request.StoreName, store[0].StoreName)

}
func (suite *StoreServiceTestSuite) TestGetStoreByOwnerShouldFail() {
	// t := suite.T()
	// t.Parallel()
	suite.T().Log("Testing Fetch store owner fails scenario ...")

	MockDBErr := errors.New("No store under this owner")
	suite.StoreRepo.EXPECT().GetOwnerStore(context.Background(), gomock.AssignableToTypeOf(model.Store{}.OwnerID)).Return([]db.Store{}, MockDBErr).Times(1)
	store, err := suite.underTest.GetOwnerStore(context.Background(), form.Request.OwnerID)
	//Assertions
	require.EqualError(suite.T(), err, MockDBErr.Error())
	require.Empty(suite.T(), store)

}
