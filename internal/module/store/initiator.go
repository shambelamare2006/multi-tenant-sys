package store

import (
	"context"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	persistence "github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence"
)

//go:generate mockgen -destination=../../../mocks/stores/mock_Storer.go -package=store_mock github.com/shambel-amare/multi-tenant-sys/internal/module/stores Storer

type storeService struct {
	Repo persistence.StoreRepository
}
type Storer interface {
	CreateStore(c context.Context, store model.Store) (model.Store, error)
	GetStoreByID(c context.Context, id uuid.UUID) (model.Store, error)
	GetOwnerStore(c context.Context, owner string) ([]model.Store, error)
}

func NewStoreService(repo persistence.StoreRepository) Storer {
	return &storeService{
		Repo: repo,
	}
}

func storeResponse(stores []db.Store) *[]model.Store {
	var modelStores []model.Store
	for _, store := range stores {
		mstore := &model.Store{
			ID:         store.ID,
			OwnerID:    store.OwnerID,
			StoreName:  store.StoreName,
			StoreType:  store.StoreType,
			StoreImage: store.StoreImage,
			Created_at: store.CreatedAt,
			Updated_at: store.UpdatedAt,
		}
		modelStores = append(modelStores, *mstore)
	}
	return &modelStores
}

func createStoreParams(store model.Store) *db.CreateStoreParams {
	return &db.CreateStoreParams{
		OwnerID:   store.OwnerID,
		StoreName: store.StoreName,
		StoreType: store.StoreType,
	}
}
func singleStoreResponse(store db.Store) *model.Store {
	return &model.Store{
		ID:         store.ID,
		OwnerID:    store.OwnerID,
		StoreName:  store.StoreName,
		StoreType:  store.StoreType,
		StoreImage: store.StoreImage,
		Created_at: store.CreatedAt,
		Updated_at: store.UpdatedAt,
	}
}
