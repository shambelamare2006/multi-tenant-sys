package role

import (
	"context"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	persistence "github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence"
)

//go:generate mockgen -destination=../../../mocks/roles/mock_RoleInfo.go -package=role_mock github.com/shambel-amare/multi-tenant-sys/internal/module/roles RoleInfo

type roleService struct {
	Repo persistence.RoleRepository
}
type RoleInfo interface {
	CreateRole(c context.Context, store model.Role) (model.Role, error)
	UpdateRoleStatus(c context.Context, id uuid.UUID, stat string) (model.Role, error)
	// UpdateRoleById(c context.Context, id uuid.UUID) (model.Role, error)

	LoggInRole(c context.Context, email string, password string) (model.Role, error)
}

func NewRoleService(repo persistence.RoleRepository) RoleInfo {
	return &roleService{
		Repo: repo,
	}
}
func Response_struct(r db.Role) model.Role {
	return model.Role{
		ID:        r.ID,
		RoleEmail: r.RoleEmail,
		TenantID:  r.TenantID,
		Title:     r.Title,
		RolePhone: r.RolePhone,
		Status:    string(r.Status),
		CreatedAt: r.CreatedAt,
		UpdatedAt: r.UpdatedAt,
	}
}

func createRoleParams(m model.Role) db.CreateRoleParams {
	Role := db.CreateRoleParams{
		Title:        m.Title,
		TenantID:     m.TenantID,
		RoleEmail:    m.RoleEmail,
		RolePhone:    m.RolePhone,
		RolePassword: m.RolePassword,
		Status:       db.Stat(m.Status),
	}
	return Role
}
func roleResponse(role db.Role) model.Role {
	return model.Role{
		ID:        role.ID,
		Title:     role.Title,
		TenantID:  role.TenantID,
		RoleEmail: role.RoleEmail,
		RolePhone: role.RolePhone,
		Status:    string(role.Status),
		CreatedAt: role.CreatedAt,
		UpdatedAt: role.UpdatedAt,
	}
}
