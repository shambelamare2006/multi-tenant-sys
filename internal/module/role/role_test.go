package role_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/role"
	role_mock "github.com/shambel-amare/multi-tenant-sys/mocks/roles"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

func TestRoleServices(t *testing.T) {
	suite.Run(t, new(RoleServiceTestSuite))
}

type RoleServiceTestSuite struct {
	suite.Suite
	RoleRepo  *role_mock.MockRoleRepository
	underTest role.RoleInfo
}

func (suite *RoleServiceTestSuite) SetupTest() {
	mockCtrl := gomock.NewController(suite.T())
	defer mockCtrl.Finish()
	suite.RoleRepo = role_mock.NewMockRoleRepository(mockCtrl)
	service := role.NewRoleService(suite.RoleRepo)
	suite.underTest = service
}

type Format struct {
	CreateRequest  model.Role
	UpdateRequest  model.Role
	CreateResponse db.Role
	UpdateResponse db.Role
}

var id = uuid.New()

var form = Format{
	CreateRequest: model.Role{
		ID:        id,
		RoleEmail: "role1@gmail.com",
		TenantID:  "tenant1",
		Title:     "manager",
		RolePhone: "0923327298",
		Status:    "Active",
	},
	UpdateRequest: model.Role{
		ID:        id,
		RoleEmail: "role1@gmail.com",
		TenantID:  "tenant1",
		Title:     "manager",
		RolePhone: "0923327298",
		Status:    "Pending",
	},
	UpdateResponse: db.Role{
		ID:        id,
		RoleEmail: "role1@gmail.com",
		TenantID:  "tenant1",
		Title:     "manager",
		RolePhone: "0923327298",
		Status:    "Pending",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	CreateResponse: db.Role{
		ID:        id,
		RoleEmail: "role1@gmail.com",
		TenantID:  "tenant1",
		Title:     "manager",
		RolePhone: "0923327298",
		Status:    "Active",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
}

func (suite *RoleServiceTestSuite) TestCreateRoleShouldPass() {
	//Store is not a Created Before
	suite.RoleRepo.EXPECT().CreateRole(gomock.Any(), gomock.Any()).Return(form.CreateResponse, nil).Times(1)
	createdRole, err := suite.underTest.CreateRole(context.Background(), form.CreateRequest)
	//Assertions
	require.Equal(suite.T(), createdRole.RoleEmail, form.CreateResponse.RoleEmail)

	require.NoError(suite.T(), err)
}

func (suite *RoleServiceTestSuite) TestUpdateStatusForRoleShouldPass() {

	suite.RoleRepo.EXPECT().GetRoleByID(context.Background(), id).Return(form.CreateResponse, nil).Times(1)
	suite.RoleRepo.EXPECT().UpdateRoleStatus(context.Background(), gomock.Any()).Return(db.StatPending, nil).Times(1)
	role, err := suite.underTest.UpdateRoleStatus(context.Background(), id, "Pending")
	//Assertions
	require.NoError(suite.T(), err)
	require.NotZero(suite.T(), role)
	require.Equal(suite.T(), "Pending", role.Status)
}
func (suite *RoleServiceTestSuite) TestUpdateStatusForRoleShouldFail() {
	//updating un registered role
	id := uuid.New()
	MockDBErr := errors.New("could not find role by this id")
	suite.RoleRepo.EXPECT().GetRoleByID(context.Background(), id).Return(db.Role{}, MockDBErr).Times(1)
	//expect no call to repo.updaterolestatus
	suite.RoleRepo.EXPECT().UpdateRoleStatus(context.Background(), gomock.Any()).Return(db.StatPending, MockDBErr).Times(0)
	role, err := suite.underTest.UpdateRoleStatus(context.Background(), id, "Blocked")
	//Assertions
	require.Error(suite.T(), err)
	require.Zero(suite.T(), role)
	require.EqualError(suite.T(), err, MockDBErr.Error())
}
