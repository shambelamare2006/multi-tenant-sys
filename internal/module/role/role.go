package role

import (
	"context"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
)

func (i *roleService) CreateRole(c context.Context, m model.Role) (model.Role, error) {
	hashPassword, _ := auth.HashPassword(m.RolePassword)
	roleparams := createRoleParams(m)
	roleparams.RolePassword = hashPassword
	createdRole, err := i.Repo.CreateRole(c, roleparams)
	if err != nil {
		return model.Role{}, err
	}

	return roleResponse(createdRole), nil
}
func (i *roleService) UpdateRoleStatus(c context.Context, id uuid.UUID, stat string) (model.Role, error) {
	role, err := i.Repo.GetRoleByID(c, id)
	if err != nil {
		return model.Role{}, err
	}
	updateParams := updateRoleStatusParams(role)
	status, err := i.Repo.UpdateRoleStatus(c, *updateParams)
	if err != nil {
		return model.Role{}, err
	}
	// err = status.Scan(role.Status)
	role.Status = db.Stat(status)
	if err != nil {
		return model.Role{}, err
	}
	return roleResponse(role), nil
}

func (s *roleService) LoggInRole(ctx context.Context, email, password string) (model.Role, error) {
	uFetched, err := s.Repo.GetRole(ctx, email)

	if err != nil {
		//wrap the error with errorx?
		return model.Role{}, apperrors.IllegalArgument.Wrap(err, "No user exists with this email, please register first")
	}

	matchErr := auth.CheckPassword(password, uFetched.RolePassword)

	if matchErr != nil {
		return model.Role{}, apperrors.IllegalArgument.Wrap(matchErr, "Invalid email and password combination")
	}
	res := Response_struct(uFetched)
	return res, nil
}
func updateRoleStatusParams(m db.Role) *db.UpdateRoleStatusParams {
	return &db.UpdateRoleStatusParams{
		ID:     m.ID,
		Status: db.Stat(m.Status),
	}
}
