package item

import (
	"context"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	persistence "github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence"
)

//go:generate mockgen -destination=../../../mocks/items/mock_ItemInfo.go -package=item_mock github.com/shambel-amare/multi-tenant-sys/internal/module/items ItemInfo

type itemService struct {
	Repo persistence.ItemRepository
}
type ItemInfo interface {
	CreateItem(c context.Context, store model.Item) (model.Item, error)
	GetItem(c context.Context, id uuid.UUID) (model.Item, error)
}

func NewItemService(repo persistence.ItemRepository) ItemInfo {
	return &itemService{
		Repo: repo,
	}
}

func itemResponse(item db.Item) model.Item {
	return model.Item{
		ID:              item.ID,
		Owner:           item.Owner,
		ItemName:        item.ItemName,
		ItemType:        item.ItemType,
		ItemDescription: item.ItemDescription,
		Status:          string(item.Status),
		CreatedAt:       item.CreatedAt,
		UpdatedAt:       item.UpdatedAt,
	}
}
func createItemParams(m model.Item) db.CreateItemParams {
	return db.CreateItemParams{
		ItemName:        m.ItemName,
		ItemType:        m.ItemType,
		Owner:           m.Owner,
		ItemDescription: m.ItemDescription,
		Status:          db.Stat(m.Status),
	}

}
