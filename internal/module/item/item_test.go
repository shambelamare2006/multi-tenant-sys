package item_test

import (
	"context"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/item"
	item_mock "github.com/shambel-amare/multi-tenant-sys/mocks/items"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

func TestItemServices(t *testing.T) {
	suite.Run(t, new(ItemServiceTestSuite))
}

type ItemServiceTestSuite struct {
	suite.Suite
	ItemRepo  *item_mock.MockItemRepository
	underTest item.ItemInfo
}

func (suite *ItemServiceTestSuite) SetupTest() {
	mockCtrl := gomock.NewController(suite.T())
	defer mockCtrl.Finish()
	suite.ItemRepo = item_mock.NewMockItemRepository(mockCtrl)
	service := item.NewItemService(suite.ItemRepo)
	suite.underTest = service
}

type Format struct {
	CreateRequest  model.Item
	UpdateRequest  model.Item
	CreateResponse db.Item
	UpdateResponse db.Item
}

var id = uuid.New()

var form = Format{
	CreateRequest: model.Item{
		ID:              id,
		Owner:           "role1@gmail.com",
		ItemName:        "item1",
		ItemType:        "fragile",
		ItemDescription: "product description",
		Status:          "Active",
	},
	UpdateRequest: model.Item{
		ID:              id,
		Owner:           "role1@gmail.com",
		ItemName:        "item1",
		ItemType:        "fragile",
		ItemDescription: "product description",
		Status:          "Blocked",
	},
	UpdateResponse: db.Item{
		ID:              id,
		Owner:           "role1@gmail.com",
		ItemName:        "item1",
		ItemType:        "fragile",
		ItemDescription: "product description",
		Status:          "Blocked",
		CreatedAt:       time.Now(),
		UpdatedAt:       time.Now(),
	},
	CreateResponse: db.Item{
		ID:              id,
		Owner:           "role1@gmail.com",
		ItemName:        "item1",
		ItemType:        "fragile",
		ItemDescription: "product description",
		Status:          "Blocked",
		CreatedAt:       time.Now(),
		UpdatedAt:       time.Now(),
	},
}

func (suite *ItemServiceTestSuite) TestCreateItemShouldPass() {
	//Store is not a Created Before
	suite.ItemRepo.EXPECT().CreateItem(gomock.Any(), gomock.Any()).Return(form.CreateResponse, nil).Times(1)
	createdItem, err := suite.underTest.CreateItem(context.Background(), form.CreateRequest)
	//Assertions
	require.Equal(suite.T(), createdItem.ItemName, form.CreateResponse.ItemName)

	require.NoError(suite.T(), err)
}
