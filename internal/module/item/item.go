package item

import (
	"context"

	"github.com/google/uuid"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
)

func (i *itemService) CreateItem(c context.Context, m model.Item) (model.Item, error) {
	itemParams := createItemParams(m)
	createdItem, err := i.Repo.CreateItem(c, itemParams)
	if err != nil {
		return model.Item{}, err
	}

	return itemResponse(createdItem), nil
}
func (i *itemService) GetItem(c context.Context, id uuid.UUID) (model.Item, error) {
	fetched, err := i.Repo.GetItem(c, id)
	if err != nil {
		return model.Item{}, err
	}
	return itemResponse(fetched), nil
}
