package tenant

import (
	"context"

	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
)

func (t tenantService) Register(ctx context.Context, m *model.Tenant) (model.Tenant, error) {

	fetchedtenant, err := t.Repo.GetTenant(ctx, m.ID)

	if err == nil && fetchedtenant.TenantName != "" {
		//wrap the error with errorx
		return model.Tenant{}, apperrors.DuplicateUser.Wrap(err, "Tanant already exist")
	}
	hashPassword, err := auth.HashPassword(m.AdminPassword)
	if err != nil {
		return model.Tenant{}, err
	}
	tenantParams := createTenantParams(*m)
	tenantParams.AdminPassword = hashPassword

	createdTenant, err := t.Repo.CreateTenant(ctx, *tenantParams)
	if err != nil {
		return model.Tenant{}, err
	}

	return tenantResponse(createdTenant), nil
}

func (t tenantService) LogginTenantAdmin(ctx context.Context, email, password string) (model.Tenant, error) {

	tenantFetched, err := t.Repo.GetTenantAdmin(ctx, email)

	if err != nil {
		//wrap the error with errorx?
		return model.Tenant{}, apperrors.IllegalArgument.Wrap(err, "No user exists with this email, please register first")
	}

	matchErr := auth.CheckPassword(password, tenantFetched.AdminPassword)

	if matchErr != nil {
		return model.Tenant{}, apperrors.IllegalArgument.Wrap(matchErr, "Invalid email and password combination")
	}

	return tenantResponse(tenantFetched), nil
}
