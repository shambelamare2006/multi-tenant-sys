package tenant

import (
	"context"

	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	persistence "github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence"
)

//go:generate mockgen -destination=../../../mocks/tenants/mock_TenantService.go -package=tenant_mock github.com/shambel-amare/multi-tenant-sys/internal/module/tenants TenantService

type tenantService struct {
	Repo persistence.TenantRepository
}
type TenantService interface {
	Register(ctx context.Context, u *model.Tenant) (model.Tenant, error)
	LogginTenantAdmin(ctx context.Context, email, password string) (model.Tenant, error)
}

func NewTenantService(repo persistence.TenantRepository) TenantService {
	return &tenantService{
		Repo: repo,
	}
}

func tenantResponse(t db.Tenant) model.Tenant {
	return model.Tenant{
		ID:         t.ID,
		TenantName: t.TenantName,
		TenantType: t.TenantType,
		AdminName:  t.AdminName,
		AdminEmail: t.AdminEmail,
		AdminPhone: t.AdminPhone,
		CreatedAt:  t.CreatedAt,
		UpdatedAt:  t.UpdatedAt,
	}
}
func createTenantParams(m model.Tenant) *db.CreateTenantParams {
	return &db.CreateTenantParams{
		TenantName:    m.TenantName,
		TenantType:    m.TenantType,
		AdminName:     m.AdminName,
		AdminEmail:    m.AdminEmail,
		AdminPhone:    m.AdminPhone,
		AdminPassword: m.AdminPassword,
	}
}
