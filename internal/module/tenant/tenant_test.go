package tenant_test

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"
	"github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/tenant"
	tenant_mock "github.com/shambel-amare/multi-tenant-sys/mocks/tenants"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

func TestTenantServices(t *testing.T) {
	suite.Run(t, new(TenantServiceTestSuite))
}

type TenantServiceTestSuite struct {
	suite.Suite
	TenantRepo *tenant_mock.MockTenantRepository
	underTest  tenant.TenantService
}

func (suite *TenantServiceTestSuite) SetupTest() {
	mockCtrl := gomock.NewController(suite.T())
	defer mockCtrl.Finish()
	suite.TenantRepo = tenant_mock.NewMockTenantRepository(mockCtrl)
	service := tenant.NewTenantService(suite.TenantRepo)
	suite.underTest = service
}

type Format struct {
	Request  *model.Tenant
	Response *db.Tenant
}
type loginFormat struct {
	Email    string
	Password string
}

var form = Format{
	Request: &model.Tenant{
		TenantName:    "Tenant1",
		TenantType:    "Pvt",
		AdminName:     "Admin tenant1",
		AdminEmail:    "admin@gmail.com",
		AdminPhone:    "0983327298",
		AdminPassword: "admin12334",
	},
	Response: &db.Tenant{
		TenantName: "Tenant1",
		TenantType: "Pvt",
		AdminName:  "Admin tenant1",
		AdminEmail: "admin@gmail.com",
		AdminPhone: "0983327298",
	},
}

func (suite *TenantServiceTestSuite) TestTenantRegisterionShouldPass() {
	//Tanant is not a registered Before
	suite.TenantRepo.EXPECT().GetTenant(context.Background(), gomock.AssignableToTypeOf(model.Tenant{}.ID)).Return(db.Tenant{}, nil).Times(1)
	suite.TenantRepo.EXPECT().CreateTenant(gomock.Any(), gomock.Any()).Return(*form.Response, nil).Times(1)
	tenant, err := suite.underTest.Register(context.Background(), form.Request)
	//Assertions
	require.Equal(suite.T(), tenant.AdminEmail, form.Response.AdminEmail)
	require.NoError(suite.T(), err)
	//assert password is not returned
	require.Empty(suite.T(), tenant.AdminPassword)
}
func (suite *TenantServiceTestSuite) TestTenantRegisterionShouldFail() {
	//Tanant is already a registered Before
	var MockDBErr error = nil
	targetErr := apperrors.DuplicateUser.Wrap(MockDBErr, "Tanant already exist")
	suite.TenantRepo.EXPECT().GetTenant(context.Background(), gomock.AssignableToTypeOf(model.Tenant{}.ID)).Return(*form.Response, nil).Times(1)
	suite.TenantRepo.EXPECT().CreateTenant(gomock.Any(), gomock.Any()).Return(db.Tenant{}, targetErr).Times(1)
	tenant, err := suite.underTest.Register(context.Background(), form.Request)
	//Assertions
	require.Error(suite.T(), err)
	require.EqualError(suite.T(), err, targetErr.Error())

	require.Zero(suite.T(), tenant)
	require.Equal(suite.T(), "", tenant.AdminEmail)
	//assert password is not returned
	require.Empty(suite.T(), tenant.AdminPassword)
}

var loginForm = loginFormat{
	Email:    form.Request.AdminEmail,
	Password: form.Request.AdminPassword,
}

func (suite *TenantServiceTestSuite) TestTenantAdminLogInShouldPass() {
	hashedPassword, _ := auth.HashPassword(form.Request.AdminPassword)
	form.Response.AdminPassword = hashedPassword

	suite.TenantRepo.EXPECT().GetTenantAdmin(context.Background(), gomock.AssignableToTypeOf(model.Tenant{}.AdminEmail)).Return(*form.Response, nil).Times(1)
	tenant, err := suite.underTest.LogginTenantAdmin(context.Background(), form.Request.AdminEmail, form.Request.AdminPassword)
	//Assertions
	require.NoError(suite.T(), err)
	require.NotZero(suite.T(), tenant)

	require.Equal(suite.T(), loginForm.Email, tenant.AdminEmail)
	//assert password is not returned
	require.Empty(suite.T(), tenant.AdminPassword)

}
func (suite *TenantServiceTestSuite) TestTenantAdminLogInShouldFail() {
	hashedPassword, _ := auth.HashPassword(form.Request.AdminPassword)
	form.Response.AdminPassword = hashedPassword

	MockDBErr := errors.New("No Tenant registered")
	targetErr := apperrors.IllegalArgument.Wrap(MockDBErr, "No user exists with this email, please register first")

	suite.TenantRepo.EXPECT().GetTenantAdmin(context.Background(), gomock.AssignableToTypeOf(model.Tenant{}.AdminEmail)).Return(db.Tenant{}, MockDBErr).Times(1)
	tenant, err := suite.underTest.LogginTenantAdmin(context.Background(), form.Request.AdminEmail, form.Request.AdminPassword)
	//Assertions
	require.Error(suite.T(), err)
	require.Zero(suite.T(), tenant)
	//assert password is not returned
	require.Empty(suite.T(), tenant.AdminEmail)
	require.EqualError(suite.T(), err, targetErr.Error())

}
