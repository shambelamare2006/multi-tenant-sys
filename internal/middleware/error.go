package middlewares

import (
	"net/http"

	"github.com/shambel-amare/multi-tenant-sys/internal/apperrors"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
)

func ErrorHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		for _, err := range c.Errors {
			status := http.StatusInternalServerError

			if errorx.HasTrait(err.Unwrap(), errorx.Timeout()) {
				status = http.StatusRequestTimeout
			}
			if errorx.HasTrait(err.Unwrap(), errorx.Duplicate()) {
				status = http.StatusConflict
			}
			if errorx.IsOfType(err, apperrors.ValidationFaild) {
				status = http.StatusBadRequest
			}
			if e, ok := err.Unwrap().(*errorx.Error); ok {

				c.JSON(status, rest_response{
					ErrData: err_response{
						ErrorMessage:     e.Error(),
						ErrorDescription: e.Message(),
					},
				})
			}
		}

	}
}

type err_response struct {
	ErrorMessage     string `json:"error_message"`
	ErrorDescription string `json:"error_description"`
}
type rest_response struct {
	ErrData err_response `json:"err_data"`
}
