package middlewares

import (
	"fmt"
	"log"
	"net/http"

	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
)

func Authenticate() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := auth.TokenValid(c.Request)
		if err != nil {
			log.Printf("Not Authenticated")

			c.JSON(http.StatusUnauthorized, "unauthorized")
			c.Abort()

			return
		}
		c.Next()
	}
}

// Authorize determines if current subject has been authorized to take an action on an object.
func Authorize(obj string, act string, adapter *sqladapter.Adapter) gin.HandlerFunc {
	return func(c *gin.Context) {
		err := auth.TokenValid(c.Request)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"message": "user hasn't logged in yet"})
			c.Abort()
			return
		}
		domain := c.Param("tenant_id")

		metadata, err := auth.ExtractTokenMetadata(c.Request)
		if err != nil {
			log.Printf("Metadata error:%v", err)

			c.JSON(http.StatusUnauthorized, "unauthorized")
			return
		}
		// Casbin enforces policy
		ok, err := enforce(metadata.UserEmail, domain, obj, act, adapter)
		log.Printf("email:%v", metadata.UserEmail)
		if err != nil {
			c.AbortWithStatusJSON(500, gin.H{"Message": "error occurred when authorizing user"})
			return
		}
		if !ok {
			c.AbortWithStatusJSON(403, gin.H{"Message": "forbidden"})
			return
		}
		log.Printf("Authorized")

		c.Next()
	}
}

func enforce(sub string, dom string, obj string, act string, adapter *sqladapter.Adapter) (bool, error) {
	// Load model configuration file and policy store adapter
	enforcer, err := casbin.NewEnforcer("config/rbac_model.conf", adapter)
	if err != nil {
		log.Printf("Enforcer recieved error:%v", err)
		return false, err

	}

	// Load policies from DB dynamically
	err = enforcer.LoadPolicy()
	if err != nil {
		return false, fmt.Errorf("failed to load policy from DB: %w", err)
	}
	// Verify
	ok, err := enforcer.Enforce(sub, dom, obj, act)
	if err != nil {
		return false, fmt.Errorf("failed to enforce policy: %w", err)
	}
	return ok, err
}
