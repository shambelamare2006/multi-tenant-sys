package apperrors

import "github.com/joomcode/errorx"

var (
	AppError         = errorx.NewNamespace("app-error")
	TimeoutElapsed   = AppError.NewType("request_timeout", errorx.Timeout())
	DuplicateUser    = AppError.NewType("duplicated_user", errorx.Duplicate())
	ResourceNotFound = AppError.NewType("resource_not_found", errorx.NotFound())
	ValidationFaild  = AppError.NewType("Validation_faild")

	IllegalState    = errorx.IllegalState
	IllegalArgument = errorx.IllegalArgument
)
