package routes

import (
	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/gin-gonic/gin"
	middlewares "github.com/shambel-amare/multi-tenant-sys/internal/middleware"
)

func SetupSystemRoutes(c HandlerConfigs, g *gin.RouterGroup, a *sqladapter.Adapter) {
	//system domain
	g.Use(middlewares.ErrorHandler())
	// if gin.Mode() != gin.TestMode {
	// 	g.Use(middlewares.Timeout(5*time.Second, apperrors.NewServiceUnavailable()))
	// }
	g.POST("/register/admin", middlewares.Authenticate(), c.Userhandler.SignUpSystemAdmin)
	g.POST("/signin/admin", c.Userhandler.SignInSystemAdmin)
	//TODO: this route should be super protected or we should somehow create the super admin on startup of the app
	g.POST("/signup/superadmin", c.Userhandler.SignUpSuperAdmin)
	g.POST("/signin/superadmin", c.Userhandler.SignInSuperAdmin)
}
