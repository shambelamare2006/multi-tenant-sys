package routes

import (
	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/gin-gonic/gin"
	middlewares "github.com/shambel-amare/multi-tenant-sys/internal/middleware"
)

func SetupTenantRoutes(c HandlerConfigs, g *gin.RouterGroup, a *sqladapter.Adapter) {
	//tenant domain
	g.Use(middlewares.ErrorHandler())
	// if gin.Mode() != gin.TestMode {
	// 	g.Use(middlewares.Timeout(5*time.Second, apperrors.NewServiceUnavailable()))
	// }
	g.POST("/signin/admin", c.TenantHandler.LogginTenantAdmin)
	g.POST("/register", middlewares.Authorize("tenants", "write", a), c.TenantHandler.RegisterTenant)
	//create role update role
	g.POST("/role/signin", c.RoleHandler.LoggInRole)
	g.POST("/:tenant_id/role/create", middlewares.Authenticate(), middlewares.Authorize("roles", "write", a), c.RoleHandler.CreateRole)
	g.POST("/:tenant_id/role/update", middlewares.Authenticate(), middlewares.Authorize("roles", "write", a), c.RoleHandler.UpdateRole)
	//create item update item
	g.POST("/tenant_id/item/create", middlewares.Authenticate(), middlewares.Authorize("items", "write", a), c.ItemHandler.CreateItem)
	g.POST("/:tenant_id/item/update", middlewares.Authenticate(), middlewares.Authorize("items", "write", a), c.ItemHandler.UpdateItem)
}
