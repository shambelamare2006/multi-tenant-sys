package routes

import (
	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/gin-gonic/gin"
	middlewares "github.com/shambel-amare/multi-tenant-sys/internal/middleware"
)

func SetupUserRoutes(c HandlerConfigs, g *gin.RouterGroup, a *sqladapter.Adapter) {

	g.Use(middlewares.ErrorHandler())
	// if gin.Mode() != gin.TestMode {
	// 	g.Use(middlewares.Timeout(5*time.Second, apperrors.NewServiceUnavailable()))
	// }
	g.POST("/signup", c.Userhandler.SignUp)
	g.POST("/signin", c.Userhandler.SignIn)
	g.POST("/store/create", middlewares.Authenticate(), c.Storehandler.CreateStore)
	g.POST("/:tenant_id/store/update", middlewares.Authenticate(), middlewares.Authorize("store", "read", a), c.Storehandler.GetStore)
}
