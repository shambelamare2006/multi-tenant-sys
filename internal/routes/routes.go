package routes

import (
	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	storehandler "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/store"
	tenanthandler "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/tenant"

	userhandler "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/user"
)

type HandlerConfigs struct {
	Userhandler   userhandler.UserHandler
	Storehandler  storehandler.StoreHandler
	TenantHandler tenanthandler.TenantHandler
	ItemHandler   tenanthandler.ItemHandler
	RoleHandler   tenanthandler.RoleHandler
}

func SetUpRouter(router *gin.Engine, a *sqladapter.Adapter, c HandlerConfigs) {
	//customers router group
	g1 := router.Group("/v1/customer")

	//system router group
	g2 := router.Group("/v1/system")

	//tenant router group
	g3 := router.Group("/v1/tenant")

	SetupUserRoutes(c, g1, a)
	SetupSystemRoutes(c, g2, a)
	SetupTenantRoutes(c, g3, a)

}
