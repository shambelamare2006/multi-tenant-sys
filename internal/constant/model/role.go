package model

import (
	"time"

	"github.com/google/uuid"
)

type Role struct {
	ID           uuid.UUID `json:"id"`
	Title        string    `json:"title"`
	TenantID     string    `json:"tenant_id"`
	RoleEmail    string    `json:"role_email"`
	RolePhone    string    `json:"role_phone"`
	RolePassword string    `json:"role_password"`
	Status       string    `json:"status"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}
