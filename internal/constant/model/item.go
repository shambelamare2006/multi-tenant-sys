package model

import (
	"time"

	"github.com/google/uuid"
)

type Item struct {
	ID              uuid.UUID `json:"id"`
	Owner           string    `json:"owner"`
	ItemName        string    `json:"item_name"`
	ItemType        string    `json:"item_type"`
	ItemDescription string    `json:"item_description"`
	Status          string    `json:"status"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
}
