package model

type Permission struct {
	Role   string
	User   string
	Tenant string
}
