package model

type Policy struct {
	Role      string
	Tenant    string
	Resources []string
	Rights    []string
}
