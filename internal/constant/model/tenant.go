package model

import (
	"time"

	"github.com/google/uuid"
)

type Tenant struct {
	ID            uuid.UUID `json:"id"`
	TenantName    string    `json:"tenant_name"`
	TenantType    string    `json:"tenant_type"`
	AdminName     string    `json:"admin_name"`
	AdminEmail    string    `json:"admin_email"`
	AdminPhone    string    `json:"admin_phone"`
	AdminPassword string    `json:"admin_password"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}
