package model

import (
	"time"

	"github.com/google/uuid"
)

type Store struct {
	ID         uuid.UUID `json:"id"`
	OwnerID    string    `json:"owner_id"`
	StoreName  string    `json:"store_name"`
	StoreType  string    `json:"store_type"`
	StoreImage string    `json:"store_image"`
	Created_at time.Time `json:"created_at"`
	Updated_at time.Time `json:"updated_at"`
}
