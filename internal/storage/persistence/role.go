package repository

import (
	"context"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
)

func (r pgRepository) GetRole(ctx context.Context, email string) (db.Role, error) {
	return r.DB.GetRole(ctx, email)
}
func (r pgRepository) CreateRole(ctx context.Context, arg db.CreateRoleParams) (db.Role, error) {
	return r.DB.CreateRole(ctx, arg)
}
func (r pgRepository) UpdateRoleStatus(ctx context.Context, arg db.UpdateRoleStatusParams) (db.Stat, error) {
	return r.DB.UpdateRoleStatus(ctx, arg)
}
func (r pgRepository) GetRoleByID(ctx context.Context, id uuid.UUID) (db.Role, error) {
	return r.DB.GetRoleById(ctx, id)
}
