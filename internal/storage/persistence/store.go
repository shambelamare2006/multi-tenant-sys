package repository

import (
	"context"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
)

func (r pgRepository) GetOwnerStore(ctx context.Context, owner string) ([]db.Store, error) {
	return r.DB.GetStoreByOwner(ctx, owner)
}
func (r pgRepository) GetStoreByID(ctx context.Context, id uuid.UUID) (db.Store, error) {
	return r.DB.GetStoreById(ctx, id)
}
func (r pgRepository) CreateStore(ctx context.Context, arg db.CreateStoreParams) (db.Store, error) {
	return r.DB.CreateStore(ctx, arg)
}
