package repository

import (
	"context"

	_ "github.com/golang/mock/mockgen/model"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
)

//go:generate mockgen -destination=../../../mocks/users/mock_UserRepository.go -package=user_mock github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence UserRepository
//go:generate mockgen -destination=../../../mocks/roles/mock_RoleRepository.go -package=role_mock github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence RoleRepository
//go:generate mockgen -destination=../../../mocks/items/mock_ItemRepository.go -package=item_mock github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence ItemRepository
//go:generate mockgen -destination=../../../mocks/tenants/mock_TenantRepository.go -package=tenant_mock github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence TenantRepository
//go:generate mockgen -destination=../../../mocks/stores/mock_StoreRepository.go -package=store_mock github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence StoreRepository

type pgRepository struct {
	DB *db.Queries
}
type ItemRepository interface {
	GetItem(ctx context.Context, id uuid.UUID) (db.Item, error)
	CreateItem(ctx context.Context, arg db.CreateItemParams) (db.Item, error)
}

type RoleRepository interface {
	GetRole(ctx context.Context, email string) (db.Role, error)
	GetRoleByID(ctx context.Context, id uuid.UUID) (db.Role, error)

	CreateRole(ctx context.Context, arg db.CreateRoleParams) (db.Role, error)
	UpdateRoleStatus(ctx context.Context, arg db.UpdateRoleStatusParams) (db.Stat, error)
}
type StoreRepository interface {
	GetOwnerStore(ctx context.Context, owner string) ([]db.Store, error)
	GetStoreByID(ctx context.Context, id uuid.UUID) (db.Store, error)

	CreateStore(ctx context.Context, arg db.CreateStoreParams) (db.Store, error)
}
type TenantRepository interface {
	GetTenant(ctx context.Context, id uuid.UUID) (db.Tenant, error)
	CreateTenant(ctx context.Context, arg db.CreateTenantParams) (db.Tenant, error)
	GetTenantAdmin(ctx context.Context, adminEmail string) (db.Tenant, error)
}
type UserRepository interface {
	GetUser(ctx context.Context, email string) (db.User, error)
	CreateUser(ctx context.Context, arg db.CreateUserParams) (db.User, error)
}

func NewItemRepository(db *db.Queries) ItemRepository {
	return &pgRepository{
		DB: db,
	}
}

func NewRoleRepository(db *db.Queries) RoleRepository {
	return &pgRepository{
		DB: db,
	}
}

func NewStoreRepository(db *db.Queries) StoreRepository {
	return &pgRepository{
		DB: db,
	}
}

func NewTenantRepository(db *db.Queries) TenantRepository {
	return &pgRepository{
		DB: db,
	}
}

func NewUserRepository(db *db.Queries) UserRepository {
	return &pgRepository{
		DB: db,
	}
}
