package repository

import (
	"context"

	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
)

func (r *pgRepository) GetUser(ctx context.Context, email string) (db.User, error) {
	return r.DB.GetUser(ctx, email)
}
func (r *pgRepository) CreateUser(ctx context.Context, user db.CreateUserParams) (db.User, error) {
	return r.DB.CreateUser(ctx, user)
}
