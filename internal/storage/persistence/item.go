package repository

import (
	"context"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
)

func (r pgRepository) GetItem(ctx context.Context, id uuid.UUID) (db.Item, error) {
	return r.DB.GetItem(ctx, id)
}
func (r pgRepository) CreateItem(ctx context.Context, arg db.CreateItemParams) (db.Item, error) {
	return r.DB.CreateItem(ctx, arg)
}
