package repository

import (
	"context"

	"github.com/google/uuid"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
)

func (r pgRepository) GetTenant(ctx context.Context, id uuid.UUID) (db.Tenant, error) {
	return r.DB.GetTenant(ctx, id)
}
func (r pgRepository) CreateTenant(ctx context.Context, arg db.CreateTenantParams) (db.Tenant, error) {
	return r.DB.CreateTenant(ctx, arg)
}

func (r pgRepository) GetTenantAdmin(ctx context.Context, email string) (db.Tenant, error) {
	return r.DB.GetTenantAdmin(ctx, email)
}
