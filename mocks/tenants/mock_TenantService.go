// Code generated by MockGen. DO NOT EDIT.
// Source: github.com/shambel-amare/multi-tenant-sys/internal/module/tenants (interfaces: TenantService)

// Package tenant_mock is a generated GoMock package.
package tenant_mock

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	model "github.com/shambel-amare/multi-tenant-sys/internal/constant/model"
)

// MockTenantService is a mock of TenantService interface.
type MockTenantService struct {
	ctrl     *gomock.Controller
	recorder *MockTenantServiceMockRecorder
}

// MockTenantServiceMockRecorder is the mock recorder for MockTenantService.
type MockTenantServiceMockRecorder struct {
	mock *MockTenantService
}

// NewMockTenantService creates a new mock instance.
func NewMockTenantService(ctrl *gomock.Controller) *MockTenantService {
	mock := &MockTenantService{ctrl: ctrl}
	mock.recorder = &MockTenantServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockTenantService) EXPECT() *MockTenantServiceMockRecorder {
	return m.recorder
}

// LogginTenantAdmin mocks base method.
func (m *MockTenantService) LogginTenantAdmin(arg0 context.Context, arg1, arg2 string) (model.Tenant, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "LogginTenantAdmin", arg0, arg1, arg2)
	ret0, _ := ret[0].(model.Tenant)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// LogginTenantAdmin indicates an expected call of LogginTenantAdmin.
func (mr *MockTenantServiceMockRecorder) LogginTenantAdmin(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "LogginTenantAdmin", reflect.TypeOf((*MockTenantService)(nil).LogginTenantAdmin), arg0, arg1, arg2)
}

// Register mocks base method.
func (m *MockTenantService) Register(arg0 context.Context, arg1 *model.Tenant) (model.Tenant, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Register", arg0, arg1)
	ret0, _ := ret[0].(model.Tenant)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Register indicates an expected call of Register.
func (mr *MockTenantServiceMockRecorder) Register(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Register", reflect.TypeOf((*MockTenantService)(nil).Register), arg0, arg1)
}
