// Code generated by MockGen. DO NOT EDIT.
// Source: github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/users (interfaces: UserHandler)

// Package handler_mock is a generated GoMock package.
package handler_mock

import (
	reflect "reflect"

	gin "github.com/gin-gonic/gin"
	gomock "github.com/golang/mock/gomock"
)

// MockUserHandler is a mock of UserHandler interface.
type MockUserHandler struct {
	ctrl     *gomock.Controller
	recorder *MockUserHandlerMockRecorder
}

// MockUserHandlerMockRecorder is the mock recorder for MockUserHandler.
type MockUserHandlerMockRecorder struct {
	mock *MockUserHandler
}

// NewMockUserHandler creates a new mock instance.
func NewMockUserHandler(ctrl *gomock.Controller) *MockUserHandler {
	mock := &MockUserHandler{ctrl: ctrl}
	mock.recorder = &MockUserHandlerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockUserHandler) EXPECT() *MockUserHandlerMockRecorder {
	return m.recorder
}

// SignIn mocks base method.
func (m *MockUserHandler) SignIn(arg0 *gin.Context) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SignIn", arg0)
}

// SignIn indicates an expected call of SignIn.
func (mr *MockUserHandlerMockRecorder) SignIn(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SignIn", reflect.TypeOf((*MockUserHandler)(nil).SignIn), arg0)
}

// SignInSuperAdmin mocks base method.
func (m *MockUserHandler) SignInSuperAdmin(arg0 *gin.Context) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SignInSuperAdmin", arg0)
}

// SignInSuperAdmin indicates an expected call of SignInSuperAdmin.
func (mr *MockUserHandlerMockRecorder) SignInSuperAdmin(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SignInSuperAdmin", reflect.TypeOf((*MockUserHandler)(nil).SignInSuperAdmin), arg0)
}

// SignInSystemAdmin mocks base method.
func (m *MockUserHandler) SignInSystemAdmin(arg0 *gin.Context) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SignInSystemAdmin", arg0)
}

// SignInSystemAdmin indicates an expected call of SignInSystemAdmin.
func (mr *MockUserHandlerMockRecorder) SignInSystemAdmin(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SignInSystemAdmin", reflect.TypeOf((*MockUserHandler)(nil).SignInSystemAdmin), arg0)
}

// SignUp mocks base method.
func (m *MockUserHandler) SignUp(arg0 *gin.Context) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SignUp", arg0)
}

// SignUp indicates an expected call of SignUp.
func (mr *MockUserHandlerMockRecorder) SignUp(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SignUp", reflect.TypeOf((*MockUserHandler)(nil).SignUp), arg0)
}

// SignUpSuperAdmin mocks base method.
func (m *MockUserHandler) SignUpSuperAdmin(arg0 *gin.Context) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SignUpSuperAdmin", arg0)
}

// SignUpSuperAdmin indicates an expected call of SignUpSuperAdmin.
func (mr *MockUserHandlerMockRecorder) SignUpSuperAdmin(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SignUpSuperAdmin", reflect.TypeOf((*MockUserHandler)(nil).SignUpSuperAdmin), arg0)
}

// SignUpSystemAdmin mocks base method.
func (m *MockUserHandler) SignUpSystemAdmin(arg0 *gin.Context) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SignUpSystemAdmin", arg0)
}

// SignUpSystemAdmin indicates an expected call of SignUpSystemAdmin.
func (mr *MockUserHandlerMockRecorder) SignUpSystemAdmin(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SignUpSystemAdmin", reflect.TypeOf((*MockUserHandler)(nil).SignUpSystemAdmin), arg0)
}
