# syntax=docker/dockerfile:1

FROM golang:1.19.0-alpine3.16

RUN apk update && apk upgrade && \
    apk add git \
    make openssh-client
EXPOSE 8080

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY . ./
RUN apk --no-cache add curl
RUN curl -fLo install.sh https://raw.githubusercontent.com/cosmtrek/air/master/install.sh \
    && chmod +x install.sh && sh install.sh && cp ./bin/air /bin/air

CMD air