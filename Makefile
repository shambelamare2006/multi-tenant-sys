DB_URL=postgresql://root:secret@localhost:26257/userdb?sslmode=disable
roach:
	cockroach sql --url 'postgresql://root@localhost:26257/defaultdb?sslmode=disable'
connect:
	docker exec -it multi-tenant-sys-roach1-1 ./cockroach sql --insecure
createdb:
	docker exec -it multi-tenant-sys-roach1-1 createdb --username=root --owner=root userdb
dropdb:
	docker exec -it postgres dropdb userdb
migrateup:
	docker run -v /home/twof/Desktop/go-tutorial/multi-tenant-sys/database/migrations:/migrations --network host migrate/migrate -path=/migrations/ -database "cockroachdb://root:secret@localhost:26257/defaultdb?sslmode=disable" up
migrateup2:
	docker run -v /home/twof/Desktop/go-tutorial/multi-tenant-sys/database/migrations:/migrations --network host migrate/migrate -path=/migrations/ -database "cockroachdb://root:secret@localhost:26257/defaultdb?sslmode=disable" up 2
migratedown:
	docker run -v /home/twof/Desktop/go-tutorial/multi-tenant-sys/database/migrations:/migrations --network host migrate/migrate -path=/migrations/ -database "cockroachdb://root:secret@localhost:26257/defaultdb?sslmode=disable" down 1

migratedownall:
	docker run -v /home/twof/Desktop/go-tutorial/multi-tenant-sys/database/migrations:/migrations --network host migrate/migrate -path=/migrations/ -database "cockroachdb://root:secret@localhost:26257/defaultdb?sslmode=disable" down -all
sqlc:
	sqlc generate
up:
	docker compose up -d --build
down:
	docker compose down
logapi:
	docker compose logs -f api
dockerup:
	export DOCKER_HOST=unix:///run/docker.sock
chown:	
	sudo chown twof /run/docker.sock
.PHONY: up down chown logapi createdb connect dropdb migrateup migrateup2 migratedown migratedownall sqlc roach dockerup