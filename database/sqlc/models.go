// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0

package db

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
	"time"

	"github.com/google/uuid"
)

type Stat string

const (
	StatActive  Stat = "Active"
	StatPending Stat = "Pending"
	StatBlocked Stat = "Blocked"
)

func (e *Stat) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = Stat(s)
	case string:
		*e = Stat(s)
	default:
		return fmt.Errorf("unsupported scan type for Stat: %T", src)
	}
	return nil
}

type NullStat struct {
	Stat  Stat
	Valid bool // Valid is true if String is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullStat) Scan(value interface{}) error {
	if value == nil {
		ns.Stat, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.Stat.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullStat) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Stat, nil
}

type CasbinRule struct {
	ID    int32          `json:"id"`
	PType string         `json:"p_type"`
	V0    sql.NullString `json:"v0"`
	V1    sql.NullString `json:"v1"`
	V2    sql.NullString `json:"v2"`
	V3    sql.NullString `json:"v3"`
	V4    sql.NullString `json:"v4"`
	V5    sql.NullString `json:"v5"`
	V6    sql.NullString `json:"v6"`
	V7    sql.NullString `json:"v7"`
}

type Item struct {
	ID              uuid.UUID `json:"id"`
	Owner           string    `json:"owner"`
	ItemName        string    `json:"item_name"`
	ItemType        string    `json:"item_type"`
	ItemDescription string    `json:"item_description"`
	Status          Stat      `json:"status"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
}

type Role struct {
	ID           uuid.UUID `json:"id"`
	Title        string    `json:"title"`
	TenantID     string    `json:"tenant_id"`
	RoleEmail    string    `json:"role_email"`
	RolePhone    string    `json:"role_phone"`
	RolePassword string    `json:"role_password"`
	Status       Stat      `json:"status"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

type Store struct {
	ID         uuid.UUID `json:"id"`
	OwnerID    string    `json:"owner_id"`
	StoreName  string    `json:"store_name"`
	StoreType  string    `json:"store_type"`
	StoreImage string    `json:"store_image"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}

type Tenant struct {
	ID            uuid.UUID `json:"id"`
	TenantName    string    `json:"tenant_name"`
	TenantType    string    `json:"tenant_type"`
	AdminName     string    `json:"admin_name"`
	AdminEmail    string    `json:"admin_email"`
	AdminPhone    string    `json:"admin_phone"`
	AdminPassword string    `json:"admin_password"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}

type User struct {
	ID        uuid.UUID `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Password  string    `json:"password"`
	Email     string    `json:"email"`
	Phone     string    `json:"phone"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
