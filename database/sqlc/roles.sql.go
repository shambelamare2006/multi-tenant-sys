// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0
// source: roles.sql

package db

import (
	"context"

	"github.com/google/uuid"
)

const createRole = `-- name: CreateRole :one
INSERT INTO roles (
  title,tenant_id,role_email,role_phone,role_password,status
) VALUES (
  $1, $2, $3, $4, $5, $6
)
RETURNING id, title, tenant_id, role_email, role_phone, role_password, status, created_at, updated_at
`

type CreateRoleParams struct {
	Title        string `json:"title"`
	TenantID     string `json:"tenant_id"`
	RoleEmail    string `json:"role_email"`
	RolePhone    string `json:"role_phone"`
	RolePassword string `json:"role_password"`
	Status       Stat   `json:"status"`
}

func (q *Queries) CreateRole(ctx context.Context, arg CreateRoleParams) (Role, error) {
	row := q.queryRow(ctx, q.createRoleStmt, createRole,
		arg.Title,
		arg.TenantID,
		arg.RoleEmail,
		arg.RolePhone,
		arg.RolePassword,
		arg.Status,
	)
	var i Role
	err := row.Scan(
		&i.ID,
		&i.Title,
		&i.TenantID,
		&i.RoleEmail,
		&i.RolePhone,
		&i.RolePassword,
		&i.Status,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const deleteAllRoles = `-- name: DeleteAllRoles :exec
DELETE FROM roles
`

func (q *Queries) DeleteAllRoles(ctx context.Context) error {
	_, err := q.exec(ctx, q.deleteAllRolesStmt, deleteAllRoles)
	return err
}

const deleteRole = `-- name: DeleteRole :exec
DELETE FROM roles
WHERE id = $1
`

func (q *Queries) DeleteRole(ctx context.Context, id uuid.UUID) error {
	_, err := q.exec(ctx, q.deleteRoleStmt, deleteRole, id)
	return err
}

const getRole = `-- name: GetRole :one
SELECT id, title, tenant_id, role_email, role_phone, role_password, status, created_at, updated_at FROM roles
WHERE role_email = $1
`

func (q *Queries) GetRole(ctx context.Context, roleEmail string) (Role, error) {
	row := q.queryRow(ctx, q.getRoleStmt, getRole, roleEmail)
	var i Role
	err := row.Scan(
		&i.ID,
		&i.Title,
		&i.TenantID,
		&i.RoleEmail,
		&i.RolePhone,
		&i.RolePassword,
		&i.Status,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const getRoleById = `-- name: GetRoleById :one
SELECT id, title, tenant_id, role_email, role_phone, role_password, status, created_at, updated_at FROM roles
WHERE id = $1
`

func (q *Queries) GetRoleById(ctx context.Context, id uuid.UUID) (Role, error) {
	row := q.queryRow(ctx, q.getRoleByIdStmt, getRoleById, id)
	var i Role
	err := row.Scan(
		&i.ID,
		&i.Title,
		&i.TenantID,
		&i.RoleEmail,
		&i.RolePhone,
		&i.RolePassword,
		&i.Status,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const updateRoleStatus = `-- name: UpdateRoleStatus :one
UPDATE roles 
SET status = $1
WHERE id=$2
RETURNING (status)
`

type UpdateRoleStatusParams struct {
	Status Stat      `json:"status"`
	ID     uuid.UUID `json:"id"`
}

func (q *Queries) UpdateRoleStatus(ctx context.Context, arg UpdateRoleStatusParams) (Stat, error) {
	row := q.queryRow(ctx, q.updateRoleStatusStmt, updateRoleStatus, arg.Status, arg.ID)
	var status Stat
	err := row.Scan(&status)
	return status, err
}
