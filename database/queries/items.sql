-- name: CreateItem :one
INSERT INTO items (
  item_name,owner,item_type,item_description,status
) VALUES (
  $1, $2, $3, $4, $5
)
RETURNING *;
-- name: GetItem :one
SELECT * FROM items
WHERE id = $1;

-- name: DeleteItem :exec
DELETE FROM items
WHERE id = $1;

-- name: DeleteAllItem :exec
DELETE FROM items;
