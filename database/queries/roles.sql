-- name: CreateRole :one
INSERT INTO roles (
  title,tenant_id,role_email,role_phone,role_password,status
) VALUES (
  $1, $2, $3, $4, $5, $6
)
RETURNING *;
-- name: GetRole :one
SELECT * FROM roles
WHERE role_email = $1;
-- name: GetRoleById :one
SELECT * FROM roles
WHERE id = $1;
-- name: DeleteRole :exec
DELETE FROM roles
WHERE id = $1;

-- name: DeleteAllRoles :exec
DELETE FROM roles;
-- name: UpdateRoleStatus :one
UPDATE roles 
SET status = $1
WHERE id=$2
RETURNING (status);