-- name: CreateStore :one
INSERT INTO stores (
  owner_id,store_name,store_type,store_image
) VALUES (
  $1, $2, $3, $4
)
RETURNING *;
-- name: GetStoreByOwner :many
SELECT * FROM stores
WHERE owner_id = $1;

-- name: GetStoreById :one
SELECT * FROM stores
WHERE id = $1;

-- name: DeleteStore :exec
DELETE FROM stores
WHERE id = $1;

-- name: GetAllStores :exec
SELECT * FROM stores;
