-- name: CreateTenant :one
INSERT INTO tenants (
  tenant_name,tenant_type,Admin_name,Admin_email,Admin_phone,Admin_password
) VALUES (
  $1, $2, $3, $4, $5, $6
)
RETURNING *;
-- name: GetTenant :one
SELECT * FROM tenants
WHERE id = $1;

-- name: GetTenantAdmin :one
SELECT * FROM tenants
WHERE Admin_email = $1;

-- name: DeleteTenant :exec
DELETE FROM tenants
WHERE id = $1;

-- name: DeleteAllTenants :exec
DELETE FROM tenants;
