CREATE TABLE IF NOT EXISTS items(
    id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    owner VARCHAR UNIQUE NOT NULL,
    item_name VARCHAR NOT NULL,
    item_type VARCHAR NOT NULL,
    item_description VARCHAR NOT NULL,
    status stat NOT NULL DEFAULT 'Active', 
    created_at TIMESTAMPTZ  NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now()
)