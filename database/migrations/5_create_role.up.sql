CREATE TYPE stat AS ENUM ('Active', 'Pending', 'Blocked');
CREATE TABLE IF NOT EXISTS roles(
    id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    title VARCHAR NOT NULL,
    tenant_id VARCHAR NOT NULL,
    role_email VARCHAR UNIQUE NOT NULL,
    role_phone VARCHAR NOT NULL,
    role_password VARCHAR NOT NULL,
    status stat NOT NULL DEFAULT 'Active', 
    created_at TIMESTAMPTZ  NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now()
)