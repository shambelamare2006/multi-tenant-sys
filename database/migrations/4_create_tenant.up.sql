CREATE TABLE IF NOT EXISTS tenants(
  id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
  tenant_name VARCHAR  NOT NULL,
  tenant_type VARCHAR  NOT NULL,
  admin_name VARCHAR NOT NULL,
  admin_email VARCHAR UNIQUE NOT NULL,
  admin_phone VARCHAR NOT NULL,
  admin_password VARCHAR NOT NULL,
  created_at TIMESTAMPTZ  NOT NULL DEFAULT now(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT now()
)