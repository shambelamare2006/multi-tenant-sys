CREATE TABLE IF NOT EXISTS casbin_rule (
  id SERIAL PRIMARY KEY,
  p_type VARCHAR  NOT NULL,
  v0 VARCHAR ,
  v1 VARCHAR ,
  v2 VARCHAR ,
  v3 VARCHAR,
  v4 VARCHAR,
  v5 VARCHAR,
  v6 VARCHAR,
  v7 VARCHAR
);
