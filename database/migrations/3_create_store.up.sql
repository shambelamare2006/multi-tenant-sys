CREATE TABLE IF NOT EXISTS stores (
  id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
  owner_id VARCHAR  NOT NULL,
  store_name VARCHAR NOT NULL,
  store_type VARCHAR NOT NULL,
  store_image VARCHAR NOT NULL,
  created_at TIMESTAMPTZ  NOT NULL DEFAULT now(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT now()
);