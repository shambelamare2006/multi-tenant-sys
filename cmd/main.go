package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/multi-tenant-sys/cmd/injector"
	"github.com/shambel-amare/multi-tenant-sys/internal/routes"
)

func main() {
	log.Println("Starting Server...")

	//initialize all depedency
	Allh, adapter := injector.InitializeHandlers()
	//setup router
	router := gin.Default()
	//initialize the router
	conf := routes.HandlerConfigs{
		Userhandler:   Allh.User,
		Storehandler:  Allh.Store,
		TenantHandler: Allh.Tenant,
		ItemHandler:   Allh.Item,
		RoleHandler:   Allh.Role,
	}
	routes.SetUpRouter(router, adapter, conf)
	srv := &http.Server{
		Addr:    ":8001",
		Handler: router,
	}
	// Graceful server shutdown - https://github.com/gin-gonic/examples/blob/master/graceful-shutdown/graceful-shutdown/server.go
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Failed to initialize server: %v\n", err)
		}
	}()

	log.Printf("Listening on port %v\n", srv.Addr)

	// Wait for kill signal of channel
	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	// This blocks until a signal is passed into the quit channel
	<-quit

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Shutdown server
	log.Println("Shutting down server...")
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server forced to shutdown: %v\n", err)
	}
}
