package injector

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	sqladapter "github.com/Blank-Xu/sql-adapter"
	"github.com/go-redis/redis/v7"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	storehandler "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/store"
	tenanthandler "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/tenant"
	userhandler "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/user"

	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
	itemService "github.com/shambel-amare/multi-tenant-sys/internal/module/item"
	roleService "github.com/shambel-amare/multi-tenant-sys/internal/module/role"
	storeService "github.com/shambel-amare/multi-tenant-sys/internal/module/store"
	tenantService "github.com/shambel-amare/multi-tenant-sys/internal/module/tenant"
	userService "github.com/shambel-amare/multi-tenant-sys/internal/module/user"

	Repo "github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence"
)

type AllHandlers struct {
	User   userhandler.UserHandler
	Store  storehandler.StoreHandler
	Tenant tenanthandler.TenantHandler
	Role   tenanthandler.RoleHandler
	Item   tenanthandler.ItemHandler
}
type AllServices struct {
	UserService   userService.LoggerRegisterer
	StoreService  storeService.Storer
	TenantService tenantService.TenantService
	RoleService   roleService.RoleInfo
	ItemService   itemService.ItemInfo
}

func InitializeHandlers() (*AllHandlers, *sqladapter.Adapter) {
	// H := injector.NewRestHandler()
	allservices, rd, tk, a := Inject()

	uh := userhandler.Initialize(&userhandler.Config{
		UserService:  allservices.UserService,
		TokenRepo:    rd,
		TokenService: tk,
		Adapter:      a,
	})
	sh := storehandler.Initialize(&storehandler.Config{
		StoreService: allservices.StoreService,
		TokenRepo:    rd,
		TokenService: tk,
		Adapter:      a,
	})
	th, rh, ih := tenanthandler.Initialize(&tenanthandler.Config{
		RoleService:   allservices.RoleService,
		ItemService:   allservices.ItemService,
		TenantService: allservices.TenantService,
		TokenRepo:     rd,
		TokenService:  tk,
		Adapter:       a,
	})
	allHanders := &AllHandlers{
		User:   uh,
		Store:  sh,
		Tenant: th,
		Role:   rh,
		Item:   ih,
	}
	return allHanders, a
}

const (
	DBdriver = "postgres"
	host     = "localhost"
	port     = 26257
	username = "root"
	password = "secret"
	dbname   = "defaultdb"
)

var cockroachInfo string = fmt.Sprintf("host=%s port=%d user=%s "+
	"password=%s dbname=%s sslmode=disable",
	host, port, username, password, dbname)

func dbConnection(database string) (*db.Queries, *sql.DB) {

	connection, err := sql.Open(DBdriver, database)
	if err != nil {
		log.Printf("error connection to db: %s", err)
		return nil, nil
	}
	conn := db.New(connection)
	return conn, connection
}

func Inject() (AllServices, auth.AuthInterface, auth.TokenInterface, *sqladapter.Adapter) {
	pconn, db := dbConnection(cockroachInfo)
	defer pconn.Close()
	// db, err := gorm.Open(postgres.Open(cockroachInfo), &gorm.Config{})

	// sqladapter.TurnOffAutoMigrate(db)
	// if err != nil {
	// 	log.Fatalf("error connecting gorm adapter:%v", err)
	// }
	a, err := sqladapter.NewAdapter(db, "postgres", "casbin_rule")

	if err != nil {
		log.Fatalf("error connecting gorm adapter:%v", err)
	}
	// a, err := sqladapter.NewAdapter("postgres", cockroachInfo)
	// a, err := sqladapter.NewAdapter("postgres", cockroachInfo, true)
	NewUserRepository := Repo.NewUserRepository(pconn)
	NewStoreRepository := Repo.NewStoreRepository(pconn)
	NewTenantRepository := Repo.NewTenantRepository(pconn)
	NewRoleRepository := Repo.NewRoleRepository(pconn)
	NewItemRepository := Repo.NewItemRepository(pconn)

	userService := userService.NewUserService(NewUserRepository)
	storeService := storeService.NewStoreService(NewStoreRepository)
	itemService := itemService.NewItemService(NewItemRepository)
	roleService := roleService.NewRoleService(NewRoleRepository)
	tenantService := tenantService.NewTenantService(NewTenantRepository)

	//redis details
	redis_host := os.Getenv("REDIS_HOST")
	redis_port := os.Getenv("REDIS_PORT")
	redis_password := os.Getenv("REDIS_PASSWORD")

	redisClient := NewRedisDB(redis_host, redis_port, redis_password)
	authService := auth.NewAuth(redisClient)
	tokenService := auth.NewToken()
	allservices := &AllServices{
		UserService:   userService,
		TenantService: tenantService,
		StoreService:  storeService,
		RoleService:   roleService,
		ItemService:   itemService,
	}
	return *allservices, authService, tokenService, a
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func NewRedisDB(host, port, password string) *redis.Client {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: password,
		DB:       0,
	})
	return redisClient
}
