package login__test

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http/httptest"
	"os"

	"github.com/go-redis/redis/v7"

	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"

	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"

	storeService "github.com/shambel-amare/multi-tenant-sys/internal/module/store"
	userService "github.com/shambel-amare/multi-tenant-sys/internal/module/user"
	Repo "github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence"

	"github.com/cucumber/godog"
	_ "github.com/lib/pq"
)

var rr = httptest.NewRecorder()

const (
	DBdriver = "postgres"
	host     = "localhost"
	port     = 26257
	username = "twof"
	password = "twof"
	dbname   = "userdb"
)

type RequestForm struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ExpectedResponse struct {
	User    db.User `json:"user"`
	Message string  `json:"message"`
}

var list = make([]RequestForm, 0)
var cockroachInfo string = fmt.Sprintf("host=%s port=%d user=%s "+
	"password=%s dbname=%s sslmode=disable",
	host, port, username, password, dbname)

func dbConnection(database string) *db.Queries {

	connection, err := sql.Open(DBdriver, database)
	if err != nil {
		log.Printf("error connection to db: %s", err)
		return nil
	}
	conn := db.New(connection)
	return conn
}
func DeleteUser(c context.Context, email string) error {
	log.Printf("Deleting----:%v", email)
	pconn := dbConnection(cockroachInfo)
	defer pconn.Close()
	err := pconn.DeleteUser(c, email)
	if err != nil {
		log.Printf("couldn't delete user with %s: error:%v", email, err)
	}
	return nil
}

func RegisterUser(c context.Context, user db.User) error {
	pconn := dbConnection(cockroachInfo)
	defer pconn.Close()
	Password, err := auth.HashPassword(user.Password)
	if err != nil {
		log.Printf("error hashing password:%v", err)
	}
	u := db.CreateUserParams{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Password:  Password,
	}
	_, dbErr := pconn.CreateUser(c, u)
	if dbErr != nil {
		log.Printf("error creating user with email %s:error:%v", u.Email, err)
	}
	return nil
}
func NewRedisDB(host, port, password string) *redis.Client {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: password,
		DB:       0,
	})
	return redisClient
}
func NewService() (userService.LoggerRegisterer, storeService.Storer, auth.AuthInterface, auth.TokenInterface) {
	pconn := dbConnection(cockroachInfo)
	defer pconn.Close()
	NewUserRepository := Repo.NewUserRepository(pconn)
	NewStoreRepository := Repo.NewStoreRepository(pconn)

	userService := userService.NewUserService(NewUserRepository)
	storeService := storeService.NewStoreService(NewStoreRepository)
	//redis details
	redis_host := os.Getenv("REDIS_HOST")
	redis_port := os.Getenv("REDIS_PORT")
	redis_password := os.Getenv("REDIS_PASSWORD")

	redisClient := NewRedisDB(redis_host, redis_port, redis_password)
	authService := auth.NewAuth(redisClient)
	tokenService := auth.NewToken()
	return userService, storeService, authService, tokenService
}
func iAmARegisteredUserWithEmailAndPassword(arg1, arg2 string) error {
	return godog.ErrPending
}
func iAmNotARegisteredUserWithEmail(arg1 string) error {
	return godog.ErrPending
}

func iLogInToTheSystemWithEmailAndPassword(arg1 *godog.Table) error {
	return godog.ErrPending
}

func iShouldGetConfirmation(arg1 string) error {
	return godog.ErrPending
}

func iShouldGetAnErrorWith(arg1 string) error {
	return godog.ErrPending
}

func InitializeScenario(Sc *godog.ScenarioContext) {
	Sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		email := sc.Steps[1].Argument.DataTable.Rows[1].Cells[0].Value
		Beforerr := DeleteUser(ctx, email)
		list = make([]RequestForm, 0)
		rr = httptest.NewRecorder()
		return ctx, Beforerr
	})
	Sc.After(func(ctx context.Context, sc *godog.Scenario, err error) (context.Context, error) {
		email := sc.Steps[1].Argument.DataTable.Rows[1].Cells[0].Value
		AfterErr := DeleteUser(ctx, email)
		return ctx, AfterErr
	})
	Sc.Step(`^I am a registered user with email "([^"]*)" and password "([^"]*)"$`, iAmARegisteredUserWithEmailAndPassword)
	Sc.Step(`^I am not a registered user with email "([^"]*)"$`, iAmNotARegisteredUserWithEmail)
	Sc.Step(`^I should get an Error with "([^"]*)"$`, iShouldGetAnErrorWith)
	Sc.Step(`^I log in to the system with email and password$`, iLogInToTheSystemWithEmailAndPassword)
	Sc.Step(`^I should get confirmation "([^"]*)"$`, iShouldGetConfirmation)
}
