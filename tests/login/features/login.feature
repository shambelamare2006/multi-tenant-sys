Feature: Log in to the system
        As a a registered user
        I can log in to the system so that 
        I can start using the system.
@registered
Scenario Outline: log in with correctly registered credentials
    Given I am a registered user with email "<email>" and password "<password>"
    When I log in to the system with email and password
    |email|password|
    |<email>|<password>|
    Then I should get confirmation "<ConfirmationOfSuccess>"
    Examples:
        |   email|          password|           ConfirmationOfSuccess|
        |   jhon@gmail.com|   secret|    successfully logedin|
   
@unregistered
Scenario Outline: log in with Unregistered credentials
    Given I am not a registered user with email "<email>"
    When I log in to the system with email and password
    |email|password|
    |<email>|<password>|
    Then I should get an Error with "<ConfirmationOfFailure>"
    Examples:
        |   email|          password|          ConfirmationOfFailure|
        |   ihon@gmail.com|  secret|   user not registered|

@wrong
Scenario Outline: log in with Wrong credentials
    Given I am not a registered user with email "<email>"
    When I log in to the system with email and password
    |email|password|
    |<email>|<password>|
    Then I should get an Error with "<ConfirmationOfFailure>"
    Examples:
        |   email|          password|          ConfirmationOfFailure|
        |   jhon@gmail.com|  notsecret|       invalid email or password|
        |   jhongmail.com|  secret|       invalid email or password|

