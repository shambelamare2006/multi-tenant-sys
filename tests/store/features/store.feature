Feature: user registration to the system
        In order to sell products
        As a user 
        I can create a store so that 
        I can register products to my store.
@new
Scenario Outline: create of new store
    Given I am a logged in with email "<Email>"  and Password "<Password>"
    When I submit the store creation form with the following details
    |Name  |Type  |Image  |
    |<Name>|<Type>|<Image>|
    Then I should get confirmation of success "<ConfirmationOfSuccess>" with status code 201
    Examples:
    |Name  |Type  |Image  |           Email|  |  Password|   ConfirmationOfSuccess|
    |mystore|liquore|image1  |   jhon@gmail.com|  secret|      store created succesfuly|
        