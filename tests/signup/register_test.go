package signup_test

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"

	sqladapter "github.com/Blank-Xu/sql-adapter"
	db "github.com/shambel-amare/multi-tenant-sys/database/sqlc"
	userhandler "github.com/shambel-amare/multi-tenant-sys/internal/handler/rest/user"

	"github.com/shambel-amare/multi-tenant-sys/internal/module/auth"
	userService "github.com/shambel-amare/multi-tenant-sys/internal/module/user"

	Repo "github.com/shambel-amare/multi-tenant-sys/internal/storage/persistence"

	"github.com/shambel-amare/multi-tenant-sys/internal/routes"

	"github.com/cucumber/godog"
	_ "github.com/lib/pq"
)

var rr = httptest.NewRecorder()

const (
	DBdriver = "postgres"
	host     = "localhost"
	port     = 26257
	username = "root"
	password = "secret"
	dbname   = "defaultdb"
)

type RequestForm struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Password  string `json:"password"`
}

type ExpectedResponse struct {
	User    db.User `json:"user"`
	Message string  `json:"message"`
}

var registeredUser = make([]RequestForm, 0)

var list = make([]RequestForm, 0)
var cockroachInfo string = fmt.Sprintf("host=%s port=%d user=%s "+
	"password=%s dbname=%s sslmode=disable",
	host, port, username, password, dbname)

func dbConnection(database string) (*db.Queries, *sql.DB) {

	connection, err := sql.Open(DBdriver, database)
	if err != nil {
		log.Printf("error connection to db: %s", err)
		return nil, nil
	}
	conn := db.New(connection)
	return conn, connection
}
func DeleteUser(c context.Context, email string) error {
	log.Printf("Deleting----:%v", email)
	pconn, _ := dbConnection(cockroachInfo)
	defer pconn.Close()
	err := pconn.DeleteUser(c, email)
	return err
}

func RegisterUser(c context.Context, user db.User) error {
	pconn, _ := dbConnection(cockroachInfo)
	defer pconn.Close()
	Password, err := auth.HashPassword(user.Password)
	if err != nil {
		log.Printf("error hashing password:%v", err)
		return err
	}
	u := db.CreateUserParams{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Phone:     user.Phone,
		Password:  Password,
	}
	log.Print("deleting test user: if exist")

	errDel := DeleteUser(c, u.Email)
	if errDel != nil {
		log.Printf("error deleting test user:%v", errDel)
		return errDel
	}
	log.Print("Creating test user:")

	_, dbErr := pconn.CreateUser(c, u)
	if dbErr != nil {
		log.Printf("error creating test user:%v", dbErr)
		return dbErr
	}
	return nil
}

func NewService() (userService.LoggerRegisterer, *sqladapter.Adapter) {
	pconn, db := dbConnection(cockroachInfo)
	defer pconn.Close()
	NewUserRepository := Repo.NewUserRepository(pconn)

	userService := userService.NewUserService(NewUserRepository)
	a, err := sqladapter.NewAdapter(db, "postgres", "casbin_rule")
	if err != nil {
		panic(err)
	}
	return userService, a
}
func iAmARegisteredUserWithTheFollowingDetails(userinfo *godog.Table) error {

	for i := 1; i < len(userinfo.Rows); i++ {
		registeredUser = append(list, RequestForm{
			Email:     userinfo.Rows[i].Cells[0].Value,
			FirstName: userinfo.Rows[i].Cells[1].Value,
			LastName:  userinfo.Rows[i].Cells[2].Value,
			Password:  userinfo.Rows[i].Cells[3].Value,
			Phone:     userinfo.Rows[i].Cells[4].Value,
		})

	}
	user := db.User{
		FirstName: registeredUser[0].FirstName,
		LastName:  registeredUser[0].LastName,
		Email:     registeredUser[0].Email,
		Phone:     registeredUser[0].Phone,
		Password:  registeredUser[0].Password,
	}
	ErrReg := RegisterUser(context.Background(), user)
	if ErrReg != nil {
		log.Printf("error registering test user:%v", ErrReg)
		return ErrReg
	}
	return nil
}

func iAmNotARegisteredUserWithEmail(email string) error {
	err := DeleteUser(context.Background(), email)
	return err
}
func iShouldGetAnErrorWithStatusCode(status int) error {
	errresponse := new(ExpectedResponse)

	// decoder := json.NewDecoder(rr.Body)
	// if err := decoder.Decode(&errresponse); err != nil {
	// 	log.Printf("error decoding recorder body to ExpectedResponse")
	// 	return err
	// }
	json.Unmarshal(rr.Body.Bytes(), &errresponse)

	log.Printf("DECODED:%v\n", errresponse)
	if rr.Code != status {
		err := errors.New("response code do not match")
		log.Printf("Want [%d], got [%d]\n", http.StatusBadRequest, rr.Code)
		return err
	}

	// if errresponse.Message != arg {
	// 	err := errors.New("error message do not match")
	// 	log.Printf("Want [%s], got [%s]\n", arg, errresponse.Message)
	// 	return err
	// }
	return nil
}

func iShouldGetConfirmationOfSuccessWithStatusCode(message string, status int) error {
	Response := new(ExpectedResponse)

	//: Use Decoding, it is faster than unmarshaling
	decoder := json.NewDecoder(rr.Body)
	if err := decoder.Decode(&Response); err != nil {
		log.Printf("error decoding recorder body to ExpectedResponse")
		return err
	}
	log.Printf("DECODED:%v\n", Response)
	if rr.Code != status {
		err := errors.New("response code do not match")
		log.Printf("Want [%d], got [%d]\n", http.StatusCreated, rr.Code)
		return err
	}
	//unmarshaling is 25% slower than Decoding
	// json.Unmarshal(rr.Body.Bytes(), &ExpectedResponse)

	// if Response.Message != arg {
	// 	err := errors.New("confirmation do not match")
	// 	log.Printf("Want [%s], got [%s]\n", arg, Response.Message)
	// 	return err
	// }
	return nil
}

func iSubmitTheRegistrationFormWithTheFollowingDetails(userinfo *godog.Table) error {
	//setup gin in testmode
	gin.SetMode(gin.TestMode)
	//set httptest response recrder
	router := gin.Default()
	userService, adapter := NewService()

	uh := userhandler.Initialize(&userhandler.Config{
		UserService: userService,
	})
	conf := routes.HandlerConfigs{
		Userhandler: uh,
	}
	routes.SetUpRouter(router, adapter, conf)
	for i := 1; i < len(userinfo.Rows); i++ {
		list = append(list, RequestForm{
			Email:     userinfo.Rows[i].Cells[0].Value,
			FirstName: userinfo.Rows[i].Cells[1].Value,
			LastName:  userinfo.Rows[i].Cells[2].Value,
			Password:  userinfo.Rows[i].Cells[3].Value,
			Phone:     userinfo.Rows[i].Cells[4].Value,
		})

	}
	// create a request body
	reqBody, err := json.Marshal(gin.H{
		"first_name": list[0].FirstName,
		"last_name":  list[0].LastName,
		"email":      list[0].Email,
		"password":   list[0].Password,
		"phone":      list[0].Phone,
	})
	if err != nil {
		log.Printf("Marshaling error")
		return err
	}

	// use bytes.NewBuffer to create a reader
	request, err := http.NewRequest(http.MethodPost, "/api/signup", bytes.NewBuffer(reqBody))
	if err != nil {
		log.Printf("Request sending error")
		return err
	}
	request.Header.Set("Content-Type", "application/json")

	router.ServeHTTP(rr, request)

	return nil
}

// func TestFeatures(t *testing.T) {
// 	suite := godog.TestSuite{
// 		ScenarioInitializer: InitializeScenario,
// 		Options: &godog.Options{
// 			Format:   "pretty",
// 			Paths:    []string{"features"},
// 			TestingT: t, // Testing instance that will run subtests.
// 		},
// 	}

// 	if suite.Run() != 0 {
// 		t.Fatal("non-zero status returned, failed to run feature tests")
// 	}
// }

func InitializeScenario(Sc *godog.ScenarioContext) {
	Sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		email := sc.Steps[1].Argument.DataTable.Rows[1].Cells[0].Value
		Err := DeleteUser(ctx, email)
		list = make([]RequestForm, 0)
		rr = httptest.NewRecorder()
		return ctx, Err
	})
	Sc.After(func(ctx context.Context, sc *godog.Scenario, err error) (context.Context, error) {
		email := sc.Steps[1].Argument.DataTable.Rows[1].Cells[0].Value
		Err := DeleteUser(ctx, email)
		return ctx, Err
	})

	Sc.Step(`^I am not a registered user with email "([^"]*)"$`, iAmNotARegisteredUserWithEmail)
	Sc.Step(`^I am a registered user with the following details$`, iAmARegisteredUserWithTheFollowingDetails)

	Sc.Step(`^I am not a registered user with email "([^"]*)"""$`, iAmNotARegisteredUserWithEmail)
	Sc.Step(`^I submit the registration form with the following details$`, iSubmitTheRegistrationFormWithTheFollowingDetails)
	Sc.Step(`^^I should get an error with status code (\d+)$`, iShouldGetAnErrorWithStatusCode)
	Sc.Step(`^I should get confirmation of success "([^"]*)" with status code (\d+)$`, iShouldGetConfirmationOfSuccessWithStatusCode)
}
