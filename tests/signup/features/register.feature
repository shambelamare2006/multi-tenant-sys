Feature: user registration to the system
        In order to log in to the system
        As a user 
        I can register to the system so that 
        I can log in to the system and use services.
@new
Scenario Outline: registeration of new user
    Given I am not a registered user with email "<Email>" 
    When I submit the registration form with the following details
    |Email  |FirstName  |LastName  |Password  |Phone|
    |<Email>|<FirstName>|<LastName>|<Password>|<Phone>|
    Then I should get confirmation of success "<ConfirmationOfSuccess>" with status code 201
    Examples:
        |            Email|  FirstName|  LastName|  Password| Phone|              ConfirmationOfSuccess|
        |   jhon@gmail.com|  jhon|       doe|        secret|    0983327298|User succesfuly registered|
        

@existing
Scenario Outline: registration of already registered user 
    Given I am a registered user with the following details
    |Email  |FirstName  |LastName  |Password  |Phone|
    |<Email>|<FirstName>|<LastName>|<Password>|<Phone>|
    When I submit the registration form with the following details
   |Email  |FirstName  |LastName  |Password  |Phone|
    |<Email>|<FirstName>|<LastName>|<Password>|<Phone>|
    Then I should get an error with status code 409 
    Examples:
        |   Email|  FirstName|  LastName|   Password|   Phone|           
        |   jane@gmail.com|  jane|  doe|   secret|0984129901|     
      
@invalid
Scenario Outline: registration with invalid or incomplete form 
    Given I am not a registered user with email "<Email>" 
    When I submit the registration form with the following details
    |Email  |FirstName  |LastName  |Password  |Phone|
    |<Email>|<FirstName>|<LastName>|<Password>|<Phone>|
    Then I should get an error with status code 400
    Examples:
        |   Email|  FirstName|  LastName|   Password|         Phone|     
        |    jhongmail.com|  jhon|       doe|        secret|        0983327298|  
        |       jhon@gmail|  jhon|       doe|        secret|          0983327298|
        |  jhon@gmail.com|  jhon|       doe|            ""|         0983327298|
        |  jhon@gmail.com|  jhon|        ""|        secret|          0983327298|
        |  jhon@gmail.com|    ""|       doe|        secret|         0983327298|
        |               ""|  jhon|       doe|        secret|        0983327298|
   